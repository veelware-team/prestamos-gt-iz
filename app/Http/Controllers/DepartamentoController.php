<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Departamento;
use App\Models\Loand;
use App\Models\Municipio;
use App\Models\Sector;
use Illuminate\Http\Request;
use Inertia\Inertia;

class DepartamentoController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:departamentos.view')->only('view');
        $this->middleware('can:departamentos.update')->only('update');
    }

    /**
     * Display a view of the resource.
     *
     * @return \Inertia\Response
     */
    public function view ()
    {
        return Inertia::render('Private/Settings/Departamentos');
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $departamentos = Departamento::select(['id','name','active'])
            ->when($request->has('active'), function ($query) use ($request) {
                $query->where('active','=', $request->active);
            })
            ->get();

        $departamentos->each(function($item){
            $existLoans = Loand::query()
                ->where('departamento_id','=', $item->id)
                ->count();
            $existCustomer = Customer::query()
                ->where('departamento_id','=', $item->id)
                ->count();
            $item->exist_loans = $existLoans;
            $item->exist_customers = $existCustomer;
        });
        return response()->json(['departamentos'=>$departamentos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Departamento  $departamento
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Departamento $departamento)
    {
        $active = false;
        if($request->active == 0){
            $active = true;
        }
        $departamento->update(['active' => $active]);

        if(!$active){
            if($departamento->municipios()->where('active','=',true)->count() > 0){
                Municipio::query()->where('active', '=', true)->update(['active' => false]);
            }
            if($departamento->sectores()->where('active','=',true)->count() > 0){
                Sector::query()->where('active', '=', true)->update(['active' => false]);
            }
        }
        return response()->json(['message'=>'El departamento ha sido actualizado.']);
    }


}
