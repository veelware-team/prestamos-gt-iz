<?php

namespace App\Http\Controllers;

use App\Models\Loand;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class PaymentController extends Controller
{

    public function __construct()
    {

        $this->middleware('can:payments.view')->only('view');
        $this->middleware('can:payments.pay')->only('individualPay');
        $this->middleware('can:payments.reversion')->only('reversion');

    }

    public function view ()
    {
        return Inertia::render('Private/Payment/PaymentIndex');
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request  $request)
    {
       $payments = Payment::query()
            ->where('correlative', '<>', '')
            ->whereBetween('payment_date', [$request->dateFrom, $request->dateTo])
            ->orderBy('correlative','DESC')
            ->paginate( (int)$request->per_page);

        $paymentsTotal = Payment::query()
            ->where('correlative', '<>', '')
            ->where('state', '=', 'PAGADO')
            ->whereBetween('payment_date', [$request->dateFrom, $request->dateTo])
            ->orderBy('correlative','DESC')
            ->sum("total_payment");

        foreach ($payments  as $payment ){
            $collector =  $payment->collector()->select(['firstName','lastName', 'username'])->first();
            if($collector){
                $collectorfirstName = explode(" ", $collector->firstName )[0];
                $collectorlastName = explode(" ", $collector->lastName )[0];
                $payment["collected_by_name"] = $collector->username." ( ".$collectorfirstName." ".$collectorlastName.")";
            }
            if($payment->nullified_at){
                $nullifier =  $payment->nullifier()->select(['firstName','lastName', 'username'])->first();
                $nullifierfirstName = explode(" ", $nullifier->firstName )[0];
                $nullifierlastName = explode(" ", $nullifier->lastName )[0];
                $payment["nullified_by_name"] = $nullifier->username." ( ".$nullifierfirstName." ".$nullifierlastName.")";

                $prevCollector =  $payment->prevCollector()->select(['firstName','lastName', 'username'])->first();
                $prevCollectorfirstName = explode(" ", $prevCollector->firstName )[0];
                $prevCollectorlastName = explode(" ", $prevCollector->lastName )[0];
                $payment["prev_collected_by_name"] = $prevCollector->username." ( ".$prevCollectorfirstName." ".$prevCollectorlastName.")";
            }
            $payment["loand_correlative"] = $payment->loand->correlative;
            $payment["customer"] = $payment->loand->customer->firstName." ".$payment->loand->customer->lastName;
            $payment["customer_id"] = $payment->loand->customer->id;
        }
       /* dd(['payments'=>$payments, 'total' => $paymentsTotal]);*/
        return response()->json(['payments'=>$payments, 'total' => $paymentsTotal]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loand  $loand
     * @param  \App\Models\Payment  $payment
     */
    public function individualPay(Request $request, Loand $loand, Payment $payment)
    {
        $request->merge([
            'collected_by' =>  Auth::id(),
            'state' =>  "PAGADO",
            'paid_out' =>  true,
        ]);

        if(!$payment->was_annulled){
            $request->merge([
                'correlative' =>  Payment::nextCorrelative(),
            ]);
        }
        $payment->update($request->all());

        $payments =  $loand->payments()->get();

        $payments_paid_out = $payments->filter(function ($item) {
            return data_get($item, 'paid_out') == true;
        })->count();

        if($payments_paid_out === $loand->dues){
            $loand->update([
                'state' => 'PAGADO',
                'completed_at' => Carbon::now()
            ]);
        }

        return response()->json([
            'loand'=> $loand,
            'payment'=> $payment,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loand  $loand
     * @param  \App\Models\Payment  $payment
     */
    public function reversion(Request $request, Loand $loand, Payment $payment)
    {

        $total = $payment->capital_payment + $payment->interest_payment;

        $request->merge([
            'nullified_by' =>  Auth::id(),
            'nullified_at' =>  Carbon::now(),
            'was_annulled' =>  true,
            'prev_collected_by' =>  $payment->collected_by,
            'prev_collection_note' =>  $payment->collection_note,
            'payment_date' =>  null,
            'collected_by' =>  null,
            'collection_note' =>  null,
            'state' =>  "REVERTIDO",
            'paid_out' =>  false,
            'extraordinary' =>  false,
            'no_late_fee' =>  false,
            'late_fee' =>  null,
            'no_interest_payment' =>  false,
            'total_payment' =>  $total,
        ]);
        $payment->update($request->all());

        if($loand->state ==='PAGADO'){
            $loand->update([
                'state' => 'ACTIVO',
                'completed_at' => null,
            ]);
        }
        return redirect(route('loands.show', $loand))
            ->with('success', 'Pago revertido');
    }

    public function totalDailyCollected()
    {
        $total_fees_collected = Payment::query()
            ->where('payment_date', '=',Carbon::now()->format('Y-m-d'))
            ->where('state','=', "PAGADO")
            ->sum('total_payment');
        return response()->json(['total_fees_collected'=> $total_fees_collected]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loand  $loand
     */
    public function paymentOfLoandPending(Loand $loand){

        //dd(Carbon::now()->endOfDay()->format('Y-m-d'));
        $payments = Payment::query()
            ->where('paid_out','=','false')
            ->where('loand_id','=',$loand->id)
            ->get();


        foreach ($payments as $key => $payment) {
            if($key === 0){
                $payments[$key]["available_to_pay"] = true;
            }else{
                $payments[$key]["available_to_pay"] = false;
            }
        }


        $payments_extraordinary = $payments->filter(function ($item) {
            return data_get($item, 'estimated_payment_date') > Carbon::now()->addDay(1)->format('Y-m-d');
        })->values();

        $payments_for_today = $payments->filter(function ($item) {
            return data_get($item, 'estimated_payment_date') < Carbon::now()->addDay(1)->format('Y-m-d');
        })->values();



        return response()->json([
            'payments_for_today'=>$payments_for_today,
            'payments_extraordinary'=>$payments_extraordinary,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loand  $loand
     */
    public function paymentOfLoand(Loand  $loand)
    {
        $payments =  $loand->payments()->get();

        $payments_paid_out = $payments->filter(function ($item) {
            return data_get($item, 'paid_out') == true;
        })->count();

        foreach ($payments  as $key => $payment) {
            $collector =  $payment->collector()->select(['firstName','lastName', 'username'])->first();
            if($collector){
                $collectorfirstName = explode(" ", $collector->firstName )[0];
                $collectorlastName = explode(" ", $collector->lastName )[0];
                $payment["collected_by_name"] = $collector->username." ( ".$collectorfirstName." ".$collectorlastName.")";
            }
            if($payment->nullified_at){
                $nullifier =  $payment->nullifier()->select(['firstName','lastName', 'username'])->first();
                $nullifierfirstName = explode(" ", $nullifier->firstName )[0];
                $nullifierlastName = explode(" ", $nullifier->lastName )[0];
                $payment["nullified_by_name"] = $nullifier->username." ( ".$nullifierfirstName." ".$nullifierlastName.")";

                /*$prevCollector =  $payment->prevCollector()->select(['firstName','lastName', 'username'])->first();
                $prevCollectorfirstName = explode(" ", $prevCollector->firstName )[0];
                $prevCollectorlastName = explode(" ", $prevCollector->lastName )[0];
                $payment["prev_collected_by_name"] = $prevCollector->username." ( ".$prevCollectorfirstName." ".$prevCollectorlastName.")";*/
            }
            if($key === $payments_paid_out){
                $payments[$key]["available_to_pay"] = true;
            }else{
                $payments[$key]["available_to_pay"] = false;
            }
        }
        return response()->json([
            'payments'=>$payments,
            'payments_paid_out'=>$payments_paid_out,
        ]);
    }
}
