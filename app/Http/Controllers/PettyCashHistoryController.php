<?php

namespace App\Http\Controllers;

use App\Models\DailyIncome;
use App\Models\DailySpend;
use App\Models\Loand;
use App\Models\Payment;
use App\Models\PettyCashHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class PettyCashHistoryController extends Controller
{
    public function __construct()
    {

         $this->middleware('can:pettyCashHistories.menu')->only('menu');
         $this->middleware('can:pettyCashHistories.view')->only(['view','index']);
         $this->middleware('can:pettyCashHistories.dailyView')->only('dailyView');
         $this->middleware('can:pettyCashHistories.close')->only('close');

    }

    public function menu ()
    {
        return Inertia::render('Private/PettyCash/PettyCashMenu');
    }

    public function view ()
    {
        return Inertia::render('Private/PettyCash/PettyCashIndex');
    }

    public function dailyView ()
    {
        return Inertia::render('Private/PettyCash/PettyCashDaily');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request  $request)
    {
        $items = PettyCashHistory::query()
            ->whereBetween('date', [$request->dateFrom, $request->dateTo])
            ->orderBy('correlative','DESC')
            ->get();
        foreach ($items as $item ){
            $openingUser =  $item->openingUser()->select(['firstName','lastName', 'username'])->first();
            if($openingUser !== null ){
                $openingUserfirstName = explode(" ", $openingUser->firstName )[0];
                $openingUserlastName = explode(" ", $openingUser->lastName )[0];
                $item["open_by_name"] = $openingUser->username." ( ".$openingUserfirstName." ".$openingUserlastName.")";
            }
            if($item->ending_balance){
                $closingUser =  $item->closingUser()->select(['firstName','lastName', 'username'])->first();
                if($closingUser !== null ){
                    $closingUserfirstName = explode(" ", $closingUser->firstName )[0];
                    $closingUserlastName = explode(" ", $closingUser->lastName )[0];
                    $item["closed_by_name"] = $closingUser->username." ( ".$closingUserfirstName." ".$closingUserlastName.")";
                }
            }
        }
        return response()->json(['items'=>$items]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->merge([
            'correlative' =>  PettyCashHistory::nextCorrelative(),
            'open_by' =>  Auth::id(),
            'date' =>  Carbon::now(),
            'state' =>  "ABIERTA",
        ]);
        $pettyCash = PettyCashHistory::create($request->all());
        return redirect(route('pettyCashHistories.dailyView'))
            ->with('success', 'Caja diaria creada');
    }


    public function check ()
    {
        $exist = false;
        $pettyCashDaily = PettyCashHistory::query()->where('date', '=', Carbon::now()->format('Y-m-d'))->first();
        $pettyCashDailyPrev = PettyCashHistory::query()
            ->where('date', '<>', Carbon::now()->format('Y-m-d'))
            ->orderBy('date','DESC')
            ->first();
        if($pettyCashDaily){
            $exist = true;
        }
        return response()->json([
            'pettyCashDaily'=>$pettyCashDaily,
            'pettyCashDailyPrev'=>$pettyCashDailyPrev,
            'exist'=>$exist,
        ]);
    }

    public function checkPending ()
    {
        $exist = false;
        $pettyCashPendingCount = PettyCashHistory::query()
            ->where('date', '<', Carbon::now()->format('Y-m-d'))
            ->where('state', '=', 'ABIERTA')
            ->count();
        if($pettyCashPendingCount>0){
            $exist = true;
        }
        return response()->json([
            'exist'=>$exist,
        ]);
    }

    /**
     * Close the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PettyCashHistory  $pettyCashHistory
     */
    public function close(Request $request, PettyCashHistory $pettyCashHistory)
    {
        $initial_balance = $pettyCashHistory->initial_balance;

        $total_loans_granted = Loand::query()
            ->where('confirmed_at', 'like', ''.$request->date.'%')
            ->where(function($q){
                $q->where('state', '=', 'APROBADO')
                    ->orWhere('state', '=','ACTIVO')
                    ->orWhere('state', '=','REFINANCIADO');
            })->sum('borrowed_amount');

        $total_fees_collected = Payment::query()
            ->where('payment_date', '=',$request->date)
            ->where('state','=', "PAGADO")
            ->sum('total_payment');

        $total_daily_income = DailyIncome::query()
            ->where('date', '=',$request->date)
            ->where('state','=', "REGISTRADO")
            ->sum('amount');

        $total_daily_withdrawals = DailySpend::query()
            ->where('date', '=',$request->date)
            ->where('state','=', "REGISTRADO")
            ->sum('amount');

        $ending_balance =
            (
                $initial_balance +
                $total_daily_income +
                $total_fees_collected
            ) - (
                $total_daily_withdrawals +
                $total_loans_granted
            );

        $request->merge([
            'closed_by' =>  Auth::id(),
            'state' =>  "CERRADA",
            'total_daily_income' =>  $total_daily_income,
            'total_fees_collected' =>  $total_fees_collected,
            'total_daily_withdrawals' =>  $total_daily_withdrawals,
            'total_loans_granted' =>  $total_loans_granted,
            'ending_balance' =>  $ending_balance,
        ]);
        $pettyCashHistory->update($request->all());
        return redirect(route('pettyCashHistories.view'))
            ->with('success', 'Caja diaria cerrada');
    }

}
