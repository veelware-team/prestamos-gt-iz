<?php

namespace App\Http\Controllers;

use App\Models\Departamento;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:settings.view')->only('view');
    }

    public function view()
    {
        return Inertia::render('Private/Settings/SettingsIndex');
    }

}
