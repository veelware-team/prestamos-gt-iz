<?php

namespace App\Http\Controllers;

use App\Models\Loand;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class LoandController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:loands.view')->only(['view','index']);
        $this->middleware('can:loands.new')->only(['create','store','refinancedCreate']);
        $this->middleware('can:loands.show')->only('show');
        $this->middleware('can:loands.update')->only(['edit','update','refinancedEdit']);
        $this->middleware('can:loands.approve')->only('approve');
        $this->middleware('can:loands.reject')->only('reject');
        $this->middleware('can:loands.nullify')->only('nullify');
        $this->middleware('can:loands.activate')->only('activate');
        $this->middleware('can:loands.destroy')->only('destroy');

        $this->middleware('can:refinancedLoands.view')->only(['refinancedIndex','viewRefinanced']);
    }

    public function view ()
    {
        return Inertia::render('Private/Loand/LoandIndex');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $pageNumber = request()->query('page', 1);
        $text_search = request()->query('search', null);
        $type = request()->query('type', null);
        $state = request()->query('state', null);
        $pageSize = request()->query('itemsPerPage', 5);

        $loands = Loand::select('loands.*')
            ->join('customers', 'loands.customer_id', '=', 'customers.id')
            ->when($type !== "TODOS" && $type !== "", function ($query) use ($type) {
                $query->where('loands.type', '=', $type);
            })
            ->when($state !== "TODOS" && $state !== "", function ($query) use ($state) {
                $query->where('loands.state', '=', $state);
            })
            ->when($text_search !== null && $text_search !== "", function ($query) use ($text_search) {
                $query->where(function ($query) use ($text_search) {
                    $query->where('loands.correlative', 'like', '%' . $text_search . '%')
                        ->orWhere('loands.business_name', 'like', '%' . $text_search . '%')
                        ->orWhere('customers.firstName', 'like', '%' . $text_search . '%')
                        ->orWhere('customers.lastName', 'like', '%' . $text_search . '%');
                });
            })
            ->orderBy('loands.id', 'desc')
            ->paginate($pageSize, ['*'], 'page', $pageNumber);


        $items = $loands->map(function ($object){
            $business = $object->business_name ? $object->business_name . " - " : "";
            return [
                'id' => $object->id,
                'created_at' => $object->created_at->format('d-m-Y'),
                'correlative' => $object->correlative,
                'customer' => $business . " " . $object->customer->firstName . " " . $object->customer->lastName,
                'type' => $object->type,
                'borrowed_amount' => $object->borrowed_amount,
                'dues' => $object->dues,
                'duesPay' => 0,
                'interest_percentage' => $object->interest_percentage . "%",
                'state' => $object->state,
                'loand_parent_id' => $object->loand_parent_id ? $object->loand_parent_id : "",
            ];
        });



        $pagination = [
            'page' => $loands->currentPage(),
            'last_page' => $loands->lastPage(),
            'itemsPerPage' => $loands->perPage(),
            'total' => $loands->total(),
            'from' => $loands->firstItem(),
            'to' => $loands->lastItem(),
        ];

        return response()->json(['loands' => $items, 'pagination' => $pagination]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Private/Loand/LoandCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $correlative = $this->generateCorrelative($request);

        $request->merge([
            'created_by' =>  Auth::id(),
            'correlative' => $correlative,
        ]);

        $message= "Prestamo creado";
        if ( $request->loand_parent_id){
            $message= "Refinanciamiento creado";
        }
        if($request->approveLoan){
            $message .= " y aprobado";
            $request->merge([
                'confirmed_by' =>  Auth::id(),
                'confirmed_at' =>  Carbon::now(),
            ]);
        }
        $loand = Loand::create($request->except([
            'first_payment_date',
            'approveLoan',
            'image_url',
        ]));

        if ($request->loand_parent_id){
            $loand_parent = Loand::find($request->loand_parent_id);
            $loand_parent->update([
                'state' => 'REFINANCIADO'
            ]);

            return redirect(route('refinancedLoands.view'))
                ->with('success', $message);
        }

        if($request->user()->can('loands.view')){
            return redirect(route('loands.view'))
                ->with('success', $message);
        }else{
            return redirect(route('dashboard'))
                ->with('success', $message);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loand  $loand
     * @return \Inertia\Response
     */
    public function show(Loand $loand)
    {
        $creator =  $loand->creator()->select(['firstName','lastName', 'username'])->first();
        $creatorfirstName = explode(" ", $creator->firstName )[0];
        $creatorlastName = explode(" ", $creator->lastName )[0];
        $loand["created_by_name"] = $creator->username." ( ".$creatorfirstName." ".$creatorlastName.")";
        $loand["customer_name"] = $loand->customer->firstName." ".$loand->customer->lastName;

        if($loand->loand_parent_id){
            $loand_parent = Loand::find($loand->loand_parent_id);
            $loand["loand_parent_correlative"] = $loand_parent->correlative;
        }

        if($loand->state === "RECHAZADO"){
            $rejector =  $loand->rejector()->select(['firstName','lastName', 'username'])->first();
            $rejectorfirstName = explode(" ", $creator->firstName )[0];
            $rejectorlastName = explode(" ", $creator->lastName )[0];
            $loand["rejected_by_name"] = $rejector->username." ( ".$rejectorfirstName." ".$rejectorlastName.")";
        }
        if($loand->state === "ANULADO"){
            $nullifier =  $loand->nullifier()->select(['firstName','lastName', 'username'])->first();
            $nullifierfirstName = explode(" ", $creator->firstName )[0];
            $nullifierlastName = explode(" ", $creator->lastName )[0];
            $loand["nullified_by_name"] = $nullifier->username." ( ".$nullifierfirstName." ".$nullifierlastName.")";
        }
        if($loand->confirmed_by){
            $confirmer =  $loand->confirmer()->select(['firstName','lastName', 'username'])->first();
            $confirmerfirstName = explode(" ", $confirmer->firstName )[0];
            $confirmerlastName = explode(" ", $confirmer->lastName )[0];
            $loand["confirmed_by_name"] = $confirmer->username." ( ".$confirmerfirstName." ".$confirmerlastName.")";
        }

        $loand["departamento_name"] = $loand->departamento->name;
        $loand["municipio_name"] = $loand->municipio->name;
        $loand["sector_name"] = $loand->sector->name;
        $loand["route_name"] = $loand->route ? $loand->route->name : "";

        return Inertia::render('Private/Loand/LoandShow',[
            'loand' => $loand,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loand  $loand
     * @return \Inertia\Response
     */
    public function edit(Loand $loand)
    {
        $customer = $loand->customer()->get()[0];
        return Inertia::render('Private/Loand/LoandEdit', [
            'loand' => $loand,
            'customer' => $customer,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loand  $loand
     */
    public function update(Request $request, Loand $loand)
    {
        $parametrosActualizar =  [];
        if( $loand->state =="PENDIENTE"){
            $parametrosActualizar = $request->except([
                'state', 'loand_parent_correlative', 'customer',
            ]);
        }else{
            $parametrosActualizar = $request->only([
                'late_fee',
                'business_name',
                'phoneOne',
                'phoneTwo',
                'route_id',
                'address',
                'referenceAddress',
            ]);
        }

        $loand->update($parametrosActualizar);

        $message= "Prestamo actualizado";

        if($loand->loand_parent_id){
            $message= "Refinanciamiento actualizado";
        }

        return redirect(route('loands.show', $loand))
            ->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loand  $loand
     */
    public function destroy(Loand $loand)
    {
        $loand->delete();
        return redirect(route('loands.view'))
            ->with('success', 'El prestamo fue eliminado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loand  $loand
     */
    public function reject( Request $request, Loand $loand)
    {
        $request->merge([
            'rejected_by' =>  Auth::id(),
            'rejected_at' =>  Carbon::now(),
            'state' =>  "RECHAZADO",
            'route_id' =>  null,
        ]);
        $loand->update($request->all());
        return redirect(route('loands.show', $loand))
            ->with('success', 'El prestamo fue rechazado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loand  $loand
     */
    public function approve( Request $request, Loand $loand)
    {
        $request->merge([
            'confirmed_by' =>  Auth::id(),
            'confirmed_at' =>  Carbon::now(),
            'state' =>  "APROBADO",
        ]);
        $loand->update($request->all());
        return redirect(route('loands.show', $loand))
            ->with('success', 'El prestamo fue aprobado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loand  $loand
     */
    public function nullify( Request $request, Loand $loand)
    {
        $request->merge([
            'nullified_by' =>  Auth::id(),
            'nullified_at' =>  Carbon::now(),
            'state' =>  "ANULADO",
            'route_id' =>  null,
        ]);
        $loand->update($request->all());
        return redirect(route('loands.show', $loand))
            ->with('success', 'El prestamo fue anulado');
    }

    public function generateCorrelative(Request $request)
    {
        $loandTypeNumber = Loand::keyTypeLoand($request->type);
        $year = Carbon::now()->format('y');
        $valueSearch = 'P-'.$loandTypeNumber."-".$year;
        $lastRoute = Loand::query()->where('correlative', 'like', $valueSearch.'%')->orderBy('correlative','DESC')->first();
        $number = 1;
        if($lastRoute){
            $numberLastRoute = explode("-",$lastRoute->correlative)[3];
            $number = (int)$numberLastRoute+1;
        }
        $numberComplete = str_pad($number, 5, "0", STR_PAD_LEFT);
        return $valueSearch.'-'.$numberComplete;
    }

    /**
     * Create payments from a specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loand  $loand
     */
    public function activate( Request $request, Loand $loand)
    {
        if ($loand->payments()->count() > 0){
            return redirect(route('loands.show', $loand))
                ->with('warning', 'Lo sentimos, Este prestamo ya esta activo y con sus cuotas creadas.');
        }

        foreach ($request->payments  as $payment ){
            $loand->payments()->create([
                "estimated_payment_date" => Carbon::createFromFormat('d/m/Y', $payment["estimated_payment_date"])->format('Y-m-d'),
                "capital_payment" => $payment["capital_payment"],
                "interest_payment" => $payment["interest_payment"],
                "total_payment" => $payment["total_payment"],
                "state" => "PENDIENTE",
            ]);
        }

        $orderInRoute = $this->getOrderInRoute($loand);
        $loand->update([
            "state"=>"ACTIVO",
            "order"=>$orderInRoute,
            "first_payment_date"=> Carbon::createFromFormat('Y-m-d',  $request->first_payment_date),
        ]);
        return redirect(route('loands.show', $loand))
            ->with('success', 'El prestamo esta activo.');
    }

    public function totalDailyGranted()
    {
        $total_loans_granted = Loand::query()
            ->where('confirmed_at', 'like', ''.Carbon::now()->format('Y-m-d').'%')
            ->where(function($q){
                $q->where('state', '=', 'APROBADO')
                    ->orWhere('state', '=','ACTIVO');
            })->sum('borrowed_amount');
        return response()->json(['total_loans_granted'=> $total_loans_granted]);
    }

    protected function getOrderInRoute(Loand $loand)
    {
        $lastOrderNum = Loand::query()
            ->where('route_id', '=', $loand->route_id)
            ->where('state', '=','ACTIVO')
            ->orderBy('order','DESC')
            ->pluck('order')
            ->first();
        $number = 1;
        if($lastOrderNum){
            $number = (int)$lastOrderNum+1;
        }
        return $number;
    }

    public function viewRefinanced ()
    {
        return Inertia::render('Private/RefinancedLoan/RefinancedLoanIndex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function refinancedCreate()
    {
        return Inertia::render('Private/RefinancedLoan/RefinancedLoanCreate');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loand  $loand
     * @return \Inertia\Response
     */
    public function refinancedEdit(Loand $loand)
    {
        $customer = $loand->customer()->get()[0];
        if($loand->loand_parent_id){
            $loand_parent = Loand::find($loand->loand_parent_id);
            $business = $loand_parent->business_name? $loand_parent->business_name." - " : "";
            $loand["loand_parent_correlative"] = $loand_parent->correlative;
            $loand["customer"] = $business." ".$loand_parent->customer->firstName." ".$loand_parent->customer->lastName;
        }
        return Inertia::render('Private/RefinancedLoan/RefinancedLoanEdit', [
            'loand' => $loand,
            'customer' => $customer,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refinancedIndex()
    {
        $loands = Loand::query()
            ->where('loand_parent_id','<>', null)
            ->orderBy('id','desc')->get()->map(function ($object){

            $business = $object->business_name? $object->business_name." - " : "";
            $loand_parent = Loand::find($object->loand_parent_id);
            return [
                'id'=>$object->id,
                'created_at'=>$object->created_at->format('d-m-Y'),
                'correlative'=>$object->correlative,
                'customer'=> $business." ".$object->customer->firstName." ".$object->customer->lastName,
                //'business_name'=>$object->business_name? $object->business_name : "",
                'type'=>$object->type,
                'borrowed_amount'=>$object->borrowed_amount,
                'dues'=>$object->dues,
                'duesPay'=> 0,
                'interest_percentage'=>$object->interest_percentage."%",
                'state'=>$object->state,
                'loand_parent_id'=>$object->loand_parent_id? $object->loand_parent_id : "",
                'loand_parent_correlative'=>$loand_parent->correlative,
                'pending_amount'=>$object->pending_amount,
            ];
        });
        //$loands = Loand::all();
        return response()->json(['loands'=>$loands]);
    }



    /**
     * Buscar prestamo.
     * @param  \Illuminate\Http\Request  $request
     */
    public function search(Request $request)
    {

        $loan = Loand::query()
            ->where('correlative', '=', $request->prev_correlative)
            ->where(function($q){
                $q->where('state', '=', 'APROBADO')
                    ->orWhere('state', '=','ACTIVO');
            })
            ->get()->map(function ($object){

                $payments_paid_out = $object->payments()
                    ->where('paid_out','=',true)
                    ->get();

                $collected_amount = $payments_paid_out->sum('total_payment');
                $pending_amount = $object->total_amount - $collected_amount;

                $business = $object->business_name? $object->business_name." - " : "";
                return [
                    'id'=>$object->id,
                    'correlative'=>$object->correlative,
                    'customer'=> $business." ".$object->customer->firstName." ".$object->customer->lastName,
                    'customer_id'=> $object->customer_id,
                    'pending_amount'=>$pending_amount,
                ];
            });
        $count = count($loan);
        $loand = null;
        $existe = false;
        if($count>0){
            $existe = true;
            $loand = $loan[0];
        }
        return response()->json(['loand'=> $loand, 'exist'=> $existe]);
    }

}
