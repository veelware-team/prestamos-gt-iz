<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Route;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Inertia\Inertia;

class CustomerController extends Controller
{
    public function __construct()
    {

         $this->middleware('can:customers.view')->only('view');
         $this->middleware('can:customers.new')->only(['create','store']);
         $this->middleware('can:customers.update')->only(['edit','update','state']);
         $this->middleware('can:customers.destroy')->only('destroy');

    }

    public function view ()
    {
        return Inertia::render('Private/Customer/CustomerIndex');
    }
    /**
     * Display a listing of the resource.
     *
     *      * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $customers = Customer::query()->orderBy('id','desc')
            ->when($request->has('active'), function (Builder $query) use ($request) {
                $query->where('active','=', $request->active);
            })
            ->get()->map(function ($object){
            return [
                'id'=>$object->id,
                'correlative'=>$object->correlative,
                'identificationNumber'=>$object->identificationNumber,
                'firstName'=>$object->firstName,
                'lastName'=>$object->lastName,
                'rating'=>$object->rating,
                'active'=>$object->active,
                'image_url' => url($object->image->url),
                'phoneOne' => $object->phoneOne,
                'phoneTwo' => $object->phoneTwo,
                'departamento_id' => $object->departamento_id,
                'municipio_id' => $object->municipio_id,
                'sector_id' => $object->sector_id,
                'address' => $object->address,
                'referenceAddress' => $object->referenceAddress,
            ];
        });

        return response()->json(['customers'=>$customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Private/Customer/CustomerCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {

        $request->merge([
            'created_by' =>  Auth::id(),
            'correlative' =>  Customer::nextCorrelative(),
        ]);
        $customer = Customer::create($request->except([
           'image', 'changeExistImage', 'image_url',
        ]));
        $url = '/images/user-'.$request->gender.'.png';
        if ( $request->hasFile('image') ) {
            $url = Storage::put('customers',$request->file('image') );
        }
        $customer->image()->create( ['url' => $url] );
        if($request->user()->can('customers.view')){
            return redirect(route('customers.view'))->with('success', 'Cliente creado');
        }else{
            return redirect(route('dashboard'))->with('success', 'Cliente creado');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Inertia\Response
     */
    public function show(Customer $customer)
    {

        $creator =  $customer->creator()->select(['firstName','lastName', 'username'])->first();
        $firstName = explode(" ", $creator->firstName )[0];
        $lastName = explode(" ", $creator->lastName )[0];


        $customer["image_url"] = url($customer->image->url);
        $customer["departamentoName"] = $customer->departamento()->select('name')->get()[0]->name;
        $customer["municipioName"] = $customer->municipio()->select('name')->get()[0]->name;
        $customer["sectorName"] = $customer->sector()->select('name')->get()[0]->name;
        $customer["created_by_name"] = $creator->username." ( ".$firstName." ".$lastName.")";
        $customer["loands"] = $customer->loands()->get();

        if( $customer->deactivator()->count() > 0 ){
            $deactivator =  $customer->deactivator()->select(['firstName','lastName', 'username'])->first();
            $firstNamedea = explode(" ", $deactivator->firstName )[0];
            $lastNamedea = explode(" ", $deactivator->lastName )[0];

            $customer["deactivated_by_name"] = $deactivator->username." ( ".$firstNamedea." ".$lastNamedea.")";
        }

        return Inertia::render('Private/Customer/CustomerShow',[
            'customer' => $customer
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Inertia\Response
     */
    public function edit(Customer $customer)
    {
        $customer["image_url"] = url($customer->image->url);
        unset($customer->deactivated_at);
        unset($customer->deactivation_note);
        unset($customer->deactivated_by);
        return Inertia::render('Private/Customer/CustomerEdit',[
            'customer' => $customer
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->update($request->except([
            'image',
            'image_url',
            'changeExistImage',
        ]));

        if($request->changeExistImage == "true"){
            if( Str::contains($customer->image->url,'customers')){
                Storage::delete($customer->image->url);
            }
            $customer->image()->delete();
            $url = '/images/user-'.$request->gender.'.png';
            if ($request->hasFile('image')){
                $url = Storage::put('customers',$request->file('image'));
            }
            $customer->image()->create(['url'=> $url]);
        }
        return redirect(route('customers.view'))
            ->with('success', 'Cliente actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     */
    public function destroy(Customer $customer)
    {
        $numberOfLoands = $customer->loands()->count();
        if($numberOfLoands>0){
            return redirect(route('customers.view'))
                ->with('warning', 'El cliente ya tiene un historial de prestamos.');
        }

        $customer->delete();

        return redirect(route('customers.view'))
            ->with('success', 'Cliente eliminado');
    }

    public function generateCorrelative(Request $request)
    {
        $year = Carbon::now()->format('y');
        $lastRoute = Customer::query()->where('correlative', 'like', '%'.$year.'%')->orderBy('correlative','DESC')->first();
        $number = 1;
        if($lastRoute){
            $numberLastRoute = explode("-",$lastRoute->correlative)[1];
            $number = (int)$numberLastRoute+1;
        }
        $correlative = 'CL'.$year.'-'.$number;
        return response()->json(['correlative'=>$correlative]);
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     */
    public function state(Request $request, Customer $customer)
    {
        if($customer->active == 1 ) {
            $request->merge([
                'deactivated_by' =>  Auth::id(),
                'deactivated_at' =>  Carbon::now(),
            ]);
        }
        $customer->update($request->all());

        return redirect(route('customers.view'))
            ->with('success', 'Cliente actualizado');
    }
}
