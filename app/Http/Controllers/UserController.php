<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function __construct()
    {

        $this->middleware('can:users.view')->only('view');
        $this->middleware('can:users.collectors')->only('collectorsAvailable');
        $this->middleware('can:users.new')->only(['create','store','generateUsername']);
        $this->middleware('can:users.update')->only(['edit','update']);
        $this->middleware('can:users.changePass')->only('destroy');

    }

    public function view ()
    {
        return Inertia::render('Private/Settings/Usuarios/Index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $rolesNoSuper = Role::whereNotIn('id', [1])->get()->pluck('id')->toArray();
        $usuarios = User::role($rolesNoSuper)->orderBy('id', 'desc')->get()->map(function ($object){
            $role =  $object->roles()->select(['id','name'])->get()[0];
            return [
                'id'=>$object->id,
                'username'=>$object->username,
                'firstName'=>$object->firstName,
                'lastName'=>$object->lastName,
                'email'=>$object->email,
                'identificationNumber'=>$object->identificationNumber,
                'address'=>$object->address,
                'phoneOne'=>$object->phoneOne,
                'phoneTwo'=>$object->phoneTwo,
                'active'=>$object->active,
                'roleName'=>$role->name,
                'role_id'=>$role->id,
                'image_url' => url($object->image->url)
            ];
        });
        return response()->json(['usuarios'=>$usuarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Private/Settings/Usuarios/UserCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->merge([
            'password' =>  Hash::make('12345'),
        ]);

        $user = User::create($request->except([
            'image', 'role_id', 'image_url', 'changeExistImage',
        ]));

        $url = '/images/default-user.png';
        if ($request->hasFile('image')){
            $url = Storage::put('users',$request->file('image'));
        }

        $user->image()->create(['url'=> $url]);
        $user->assignRole($request->role_id);
        return redirect(route('users.view'))->with('success', 'usuario creado');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Inertia\Response
     */
    public function edit(User $user)
    {
        $role =  $user->roles()->select(['id','name'])->get()[0];
        $user->setAttribute('roleName', $role->name);
        $user->setAttribute('role_id', $role->id);
        $user->setAttribute('image_url', url($user->image->url));
        return Inertia::render('Private/Settings/Usuarios/UserEdit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     */
    public function update(Request $request, User $user)
    {

        $user->update($request->except([
            'image', 'role_id', 'image_url','changeExistImage'
        ]));

        if($request->changeExistImage == "true"){
            if( Str::contains($user->image->url,'users')){
                Storage::delete($user->image->url);
            }
            $user->image()->delete();
            $url = '/images/default-product.png';
            if ($request->hasFile('image')){
                $url = Storage::put('users',$request->file('image'));
            }
            $user->image()->create(['url'=> $url]);
        }
        $user->syncRoles([$request->role_id]);

        return redirect(route('users.view'))->with('success', 'usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function availableRoles()
    {
        $roles = Role::query()->select(['id','name'])->where('id','<>', 1)->get();
        return response()->json(['roles'=>$roles]);
    }

    public function generateUsername(Request $request)
    {
        $username = "";
        if ($request->id) {
            $user = User::find($request->id);
            $role = $user->roles()->select(['id', 'name'])->get()[0];
            if ($role->id == $request->role_id) {
                $username = $user->username;
                return response()->json(['username' => $username]);
            }
        }
        $rolesTrabajadores = Role::whereNotIn('id', [1, 2])->get()->pluck('name')->toArray();
        if (in_array($request->roleName, $rolesTrabajadores)) {
            $number = 1;
            $lastUser = User::query()->where('username','like', '%'.$request->roleName.'%')->orderBy( 'username','DESC')->first();
            if($lastUser){
                $numberLastUser = explode("_", $lastUser)[1];
                $number = (int)$numberLastUser+1;
            }
            $username = $request->roleName."_".$number;
        }else{
            $fName = explode(" ", $request->firstName );
            $lastName = explode(" ", $request->lastName );
            $usernameTemp = strtolower(mb_substr($fName[0],0,1) . $lastName[0]);
            $numberUserExist = User::query()->where('username', 'like','%'.$usernameTemp.'%')->count();
            $numberUserExist > 0 ? $username = $usernameTemp.$numberUserExist : $username = $usernameTemp;
        }
        return response()->json(['username'=>$username]);
    }

    public function collectorsAvailable()
    {
        $cobradores =  User::role(4)->where('active','=', 1)->get()->map(function ($object){
            return [
                'id'=>$object->id,
                'username'=>$object->username,
                'firstName'=>$object->firstName,
                'lastName'=>$object->lastName,
                'active'=>$object->active,
            ];
        });
        return response()->json(['cobradores'=>$cobradores]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function changePass(Request $request)
    {
        $password_changed = false;
        $message = "La contraseña que escribiste no es la actual.";
        $esValida = false;
        $user = $request->user();
        if ($user && Hash::check($request->password, $user->password)) {
            $esValida = true;
        }
        if($esValida){
            $message = "La contraseña ha sido cambiada.";
            $password_changed = true;
            $user->update([
                'password' => Hash::make($request->new_password),
                'personal_password' => true
            ]);
        }
        return response()->json([
            'password_changed'=>$password_changed,
            'message'=>$message,
        ]);
    }

    /**
     * resetPass the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function resetPass(Request $request)
    {
        $password_reseted = false;
        $message = "La contraseña no se pudo restablecer";
        $user = User::find($request->userId);

        if($user){
            $password_reseted = $user->update(['password' => Hash::make('12345'),'personal_password' => false]);
            if($password_reseted){
                $message = "La contraseña ha sido restablecida.";
            }
        }

        return response()->json([
            'password_reseted'=>$password_reseted,
            'message'=>$message,
        ]);
    }
}
