<?php

namespace App\Http\Controllers;

use App\Models\Loand;
use App\Models\Payment;
use App\Models\PettyCashHistory;
use App\Models\Route;
use App\Models\RouteCheckHistory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class RouteCheckHistoryController extends Controller
{

    public function __construct()
    {

        $this->middleware('can:routeCheckHistories.menu')->only('menu');
        $this->middleware('can:routeCheckHistories.view')->only(['view','index']);
        $this->middleware('can:routeCheckHistories.routesAvailables')->only('routesAvailables');

        $this->middleware('can:routeCheckHistories.checkSingle')->only(['checkSingle','checkSingleValues']);
        $this->middleware('can:routeCheckHistories.new')->only('store');

    }

    public function menu ()
    {
        return Inertia::render('Private/RouteCheck/RouteCheckMenu');
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request  $request)
    {
        $items = RouteCheckHistory::query()
            ->whereBetween('date', [$request->dateFrom, $request->dateTo])
            ->orderBy('correlative','DESC')
            ->get();

        foreach ($items as $item ){
            $collector =  $item->collector()->select(['firstName','lastName', 'username'])->first();
            if($collector){
                $collectorfirstName = explode(" ", $collector->firstName )[0];
                $collectorlastName = explode(" ", $collector->lastName )[0];
                $item["collected_by_name"] = $collector->username." ( ".$collectorfirstName." ".$collectorlastName.")";
            }
            $closingUser =  $item->closingUser()->select(['firstName','lastName', 'username'])->first();
            if($closingUser){
                $closingUserfirstName = explode(" ", $closingUser->firstName )[0];
                $closingUserlastName = explode(" ", $closingUser->lastName )[0];
                $item["closed_by_name"] = $closingUser->username." ( ".$closingUserfirstName." ".$closingUserlastName.")";
            }

            $item["route_correlative"] = $item->route->correlative;
        }
        return response()->json(['items'=>$items]);

    }


    public function routesAvailables ()
    {
        return Inertia::render('Private/RouteCheck/RouteCheckAvailables');
    }

    public function view ()
    {
        return Inertia::render('Private/RouteCheck/RouteCheckIndex');
    }
    /**
     * Check the specified resource from storage.
     *
     * @param  \App\Models\Route  $route
     */
    public function checkSingle (Route $route)
    {

        $route['cobradorName'] = $route->cobrador->firstName." ".$route->cobrador->lastName;
        return Inertia::render('Private/RouteCheck/RouteCheckSingle', [
            'routeParam' => $route,
        ]);
    }

    /**
     * Check the specified resource from storage.
     *
     * @param  \App\Models\Route  $route
     */
    public function checkSingleValues (Route $route)
    {
        $dataToCollect = $this->routeSingleDataToCollect($route);
        $dataCollected = $this->routeSingleDataCollected($route);
        $payments = $this->routeSingleAllPayments($route);
        return response()->json([
            'amount_to_collect'=> $dataToCollect['amount_to_collect'],
            'fees_to_collect'=> $dataToCollect['fees_to_collect'],
            'amount_collected'=> $dataCollected['amount_collected'],
            'fees_collected'=> $dataCollected['fees_collected'],
            'net_amount_collected'=> $dataCollected['net_amount_collected'],
            'late_fee_amount_collected'=> $dataCollected['late_fee_amount_collected'],
            'amount_collected_extraordinary'=> $dataCollected['amount_collected_extraordinary'],
            'fees_collected_extraordinary'=> $dataCollected['fees_collected_extraordinary'],
            'payments'=> $payments,
        ]);

    }


    /**
     * @param  \App\Models\Route  $route
     */
    private function routeSingleDataToCollect(Route $route)
    {
        $pagosDiarios = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('estimated_payment_date', '=', Carbon::now()->format('Y-m-d'))
            //->where('payments.payment_date', '=',Carbon::now()->subDay()->format('Y-m-d'))
            ->where('loands.route_id', '=', $route->id)
            ->where('payments.paid_out', '=',false)
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);

        $cantidadPagosDiarios =  $pagosDiarios->count();

        $totalDiario = 0;
        foreach ($pagosDiarios as $item){
            $montoPrestamo = $item->capital_payment + $item->interest_payment;
            $totalDiario += $montoPrestamo;
        }

        $pagosDiariosPagados = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '=', Carbon::now()->format('Y-m-d'))
            ->where('payments.payment_date', '=',Carbon::now()->format('Y-m-d'))
            //->where('payments.estimated_payment_date', '=', Carbon::now()->subDay()->format('Y-m-d'))
            //->where('payments.payment_date', '=',Carbon::now()->subDay()->format('Y-m-d'))
            ->where('loands.route_id', '=', $route->id)
            ->where('payments.paid_out', '=',true)
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);

        $cantidadPagosDiarios += $pagosDiariosPagados->count();

        foreach ($pagosDiariosPagados as $item){
            $montoPrestamo = $item->capital_payment + $item->interest_payment;
            $totalDiario += $montoPrestamo;
        }


        //dd($totalDiario);

        // pagos atrasados
        $pagosAtrasado = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<',Carbon::now()->format('Y-m-d'))
            //->where('payments.estimated_payment_date', '<',Carbon::now()->subDay()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->where('loands.route_id', '=', $route->id )
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);
        $cantidadPagosAtrasados = $pagosAtrasado->count();

        //dd($pagosAtrasado->count());

        $totalPagosAtrasado = 0;
        foreach ($pagosAtrasado as $item){
            $mora = $item->loand_late_fee ? $item->loand_late_fee : 0 ;
            $montoPrestamo = $item->capital_payment + $item->interest_payment + $mora;
            $totalPagosAtrasado += $montoPrestamo;
        }

        //dd($totalPagosAtrasado);
        // pagos atrasados pagados
        $pagosAtrasadoPagado = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<',Carbon::now()->format('Y-m-d'))
            ->where('payments.payment_date', '=',Carbon::now()->format('Y-m-d'))
            //->where('payments.estimated_payment_date', '<',Carbon::now()->subDay()->format('Y-m-d'))
            //->where('payments.payment_date', '=',Carbon::now()->subDay()->format('Y-m-d'))
            ->where('payments.paid_out', '=',true)
            ->where('loands.route_id', '=', $route->id )
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee'
            ]);

        $cantidadPagosAtrasadoPagado = $pagosAtrasadoPagado->count();

        $totalDiarioAtrasadoPagado = 0;
        foreach ($pagosAtrasadoPagado as $item){
            $mora = $item->loand_late_fee ? $item->loand_late_fee : 0 ;
            $montoPrestamo = $item->capital_payment + $item->interest_payment + $mora;
            $totalDiarioAtrasadoPagado += $montoPrestamo;
        }

        //dd($totalDiarioAtrasadoPagado);
        $total_sum = $totalDiario + $totalPagosAtrasado + $totalDiarioAtrasadoPagado;
        $cantidad_total = $cantidadPagosDiarios + $cantidadPagosAtrasados + $cantidadPagosAtrasadoPagado;

        return [
            'amount_to_collect' => $total_sum ,
            'fees_to_collect' => $cantidad_total
        ];
    }


    /**
     * @param  \App\Models\Route  $route
     */
    private function routeSingleDataCollected(Route $route)
    {

        $cantidadPagosRecolectados = 0;
        $cantidadPagosRecolectadosExtraordinarios = 0;
        $totalNeto = 0;
        $totalMora = 0;
        $totalPagos = 0;
        $totalPagosExtraordinarios = 0;

        $pagosRecolectados = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.payment_date', '=',Carbon::now()->format('Y-m-d'))
            //->where('payments.payment_date', '=',Carbon::now()->subDay()->format('Y-m-d'))
            ->where('payments.state','=', "PAGADO")
            ->where('loands.route_id', '=', $route->id)
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'payments.late_fee',
                'payments.total_payment',
                'payments.extraordinary',
            ]);

        foreach ($pagosRecolectados as $item){

            $mora = $item->late_fee ? $item->late_fee : 0 ;
            $montoPrestamo = $item->capital_payment + $item->interest_payment;
            $totalMora += $mora;
            $totalNeto += $montoPrestamo;
            $totalPagos += $item->total_payment;

            if( $item->extraordinary ){
                $cantidadPagosRecolectadosExtraordinarios++;
                $totalPagosExtraordinarios += $item->total_payment;
            }
        }

        $cantidadPagosRecolectados += $pagosRecolectados->count();

        return [
            'amount_collected' => $totalPagos,
            'fees_collected' => $cantidadPagosRecolectados,
            'net_amount_collected' => $totalNeto ,
            'late_fee_amount_collected' => $totalMora,
            'amount_collected_extraordinary' => $totalPagosExtraordinarios,
            'fees_collected_extraordinary' => $cantidadPagosRecolectadosExtraordinarios,
        ];
    }


    /**
     * @param  \App\Models\Route  $route
     */
    private function routeSingleAllPayments(Route $route)
    {

        $route_id = $route->id;
        $payments = DB::select( DB::raw(
            "SELECT
                    payments.id,
                    payments.correlative,
                    payments.paid_out,
                    payments.estimated_payment_date,
                    payments.payment_date,
                    payments.total_payment,
                    payments.no_late_fee,
                    payments.no_interest_payment,
                    payments.extraordinary,
                    payments.collected_by,
                    loands.correlative AS loand_correlative,
                    loands.id AS loand_id,
                    loands.late_fee AS loand_late_fee,
                    customers.firstName as customer_firstName,
                    customers.lastName as customer_lastName,
                    departamentos.name as departamento,
                    municipios.name as municipio,
                    sectors.name as sector,
                    loands.address,
                    loands.referenceAddress,
                    loands.route_id
                    FROM payments
                    INNER JOIN loands ON payments.loand_id = loands.id
                    INNER JOIN customers ON loands.customer_id = customers.id
                    INNER JOIN departamentos ON loands.departamento_id = departamentos.id
                    INNER JOIN municipios ON loands.municipio_id = municipios.id
                    INNER JOIN sectors ON loands.sector_id = sectors.id
                    WHERE
                    ( payments.estimated_payment_date <= :hoy1 AND payments.paid_out = false AND loands.route_id = :route_id )
                    OR ( payments.payment_date = :hoy2 AND payments.paid_out = true  AND loands.route_id = :route_id2)"
        ),[
            'route_id' => $route_id,
            'route_id2' => $route_id,
            'hoy1' => Carbon::now()->format('Y-m-d'),
            'hoy2' => Carbon::now()->format('Y-m-d'),
            //'hoy1' => Carbon::now()->subDay()->format('Y-m-d'),
            //'hoy2' => Carbon::now()->subDay()->format('Y-m-d'),
        ]);


        foreach ($payments  as $payment ){
            if($payment->collected_by !== null){
                $collector = User::query()->where('id','=', $payment->collected_by)->select(['firstName','lastName', 'username'])->first();
                $collectorfirstName = explode(" ", $collector->firstName )[0];
                $collectorlastName = explode(" ", $collector->lastName )[0];
                $payment->collected_by_name = $collector->username." ( ".$collectorfirstName." ".$collectorlastName.")";
            }else{
                $payment->collected_by_name = "";
            }
        }

        return $payments;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        //Dia actual
        /*'closed_by' =>  Auth::id(),
        'date' =>  Carbon::now(),*/

        //Dia anterior
        /*'closed_by' =>  3,
        'date' =>  Carbon::now()->subDay(),*/

        $request->merge([
            'correlative' => $this->nextCorrelative($request->route_correlative),
            'closed_by' =>  Auth::id(),
            'date' =>  Carbon::now(),
            'state' =>  "GUARDADO",
        ]);

        $routeCheckHistory = RouteCheckHistory::create($request->except([
            'payments','route_correlative',
        ]));

        $route = Route::find($request->route_id);
        $route->update(['state' => 'CUADRADA']);

        $idPayments = array_column($request->payments, 'id');
        $payments = Payment::find($idPayments);
        foreach ($payments  as $payment ){
            $payment->update(['route_check_history_id' => $routeCheckHistory->id]);
        }

        $idLoans = array_unique(array_column($request->payments, 'loand_id'));
        $loans = Loand::find($idLoans);
        foreach ($loans  as $loan ){
            $loan->update(['visited' => false]);
        }

        return redirect(route('routeCheckHistories.routesAvailables'))
            ->with('success', 'Historial guardado');
    }

    public function checkIfHistoryExist(Route $route)
    {

        $existHistory = RouteCheckHistory::where('date','=', Carbon::now()->format('Y-m-d'))
            ->where('route_id','=', $route->id)
            ->exists();
        return response()->json([
            'exist'=> $existHistory,
        ]);
    }

    public function checkDailyHistories()
    {
        $routes = Route::all();
        $existHistory = true;
        $pendingRoutes = array();
        foreach ( $routes as $route){
            $routeCheckHistoryExist = RouteCheckHistory::query()
                ->where('route_id', '=', $route->id)
                ->where('date', '=', Carbon::now()->format('Y-m-d'))
                ->exists();
            if(!$routeCheckHistoryExist){
                $existHistory = false;
                array_push($pendingRoutes, $route->correlative." ".$route->name);
            }
        }

        return response()->json([
            'exist'=> $existHistory,
            'pendingRoutes'=> $pendingRoutes,
        ]);
    }


    private  function nextCorrelative($route_correlative){
        //HCR-21-05-14
        $date = Carbon::now()->format('y-m-d');
        $correlative = "HCR-".$route_correlative."-".$date;
        return $correlative;
    }
}
