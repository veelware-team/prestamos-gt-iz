<?php

namespace App\Http\Controllers;

use App\Models\Loand;
use App\Models\Payment;
use App\Models\Route;
use App\Models\RouteCheckHistory;
use App\Models\Sector;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use function MongoDB\BSON\toJSON;
use Illuminate\Support\Facades\Auth;

class RouteController extends Controller
{
    public function __construct()
    {

        $this->middleware('can:routes.view')->only('view');
        $this->middleware('can:routes.new')->only(['create','store','generateCorrelative']);
        $this->middleware('can:routes.update')->only(['edit','update']);
        $this->middleware('can:routes.destroy')->only('destroy');
        $this->middleware('can:routes.sort')->only(['sort','storeSort']);
    }

    public function view ()
    {
        return Inertia::render('Private/Route/RouteIndex');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $routes = Route::all()->map(function ($object){
            $cobrador = $object->cobrador()->select(['firstName','lastName'])->get()[0];
            return [
                'id'=>$object->id,
                'correlative'=>$object->correlative,
                'name'=>$object->name,
                'state'=>$object->state,
                'departamento_id'=>$object->departamento_id,
                'departamentoName'=>$object->departamento()->select('name')->get()[0]->name,
                'municipio_id'=>$object->municipio_id,
                'municipioName'=>$object->municipio()->select('name')->get()[0]->name,
                'cobrador_id'=>$object->cobrador_id,
                'cobradorName'=> $cobrador->firstName." ".$cobrador->lastName,
                'sectores'=>  $object->sectors()->get()->pluck('id')->toArray(),
                'sectoresName'=>  $object->sectors()->get()->pluck('name')->toArray(),
            ];
        });
        return response()->json(['routes'=>$routes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Private/Route/RouteCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $route =  Route::create($request->except(['sectores']));
        $route->sectors()->attach($request->sectores);
        return redirect(route('routes.view'))->with('success', 'Ruta creada');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Route  $route
     * @return \Inertia\Response
     */
    public function edit(Route $route)
    {
        $route['sectores'] = $route->sectors()->get()->pluck('id')->toArray();
        return Inertia::render(
            'Private/Route/RouteEdit',
            [
                'routeParam' => $route
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Route  $route
     */
    public function update(Request $request, Route $route)
    {
        $route->update($request->only([
            'name',
            'cobrador_id'
        ]));
        $route->sectors()->sync($request->sectores);
        return redirect(route('routes.view'))->with('success', 'Ruta actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Route  $route
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Route $route)
    {
        $type= "success";
        $message="la ruta ha sido eliminada.";
        $existLoans = Loand::query()
            ->where('route_id','=', $route->id)
            ->count();
        if($existLoans>0){
            $type= "warning";
            $message="No puedes eliminar esta ruta ya que hay prestamos con ella. (pendientes, aprobados o activos).";
        }else{
           $route->delete();
        }

        return response()->json([
            'type'=> $type,
            'message'=> $message,
        ]);
    }

    public function generateCorrelative(Request $request)
    {
        $year = Carbon::now()->format('y');
        $lastRoute = Route::query()->where('correlative', 'like', '%'.$year.'%')->orderBy('correlative','DESC')->first();
        $number = 1;
        if($lastRoute){
            $numberLastRoute = explode("-",$lastRoute->correlative)[1];
            $number = (int)$numberLastRoute+1;
        }
        $correlative = 'RT'.$year.'-'.$number;
        return response()->json(['correlative'=>$correlative]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Route  $route
     */
    public function sort (Route $route)
    {
        $loans = Loand::query()
            ->where('route_id','=', $route->id)
            ->where('state','=', "ACTIVO")
            ->orderBy('order','ASC')
            ->get();

        $loans->each(function($item){
            $business = $item->business_name? $item->business_name." - " : "";
            $item->customer_name = $business." ".$item->customer->firstName." ".$item->customer->lastName;
        });

        $loansCount = count($loans);

        return Inertia::render('Private/Route/RouteSort', [
            'routeParam' => $route,
            'loans' => $loans,
            'loansCount' => $loansCount,
        ]);
    }

    public function storeSort(Request $request, Route $route){
        foreach ($request->arraySort as $item) {
           $loand = Loand::find($item['id']);
           $loand->update([
               'order' => $item['order']
           ]);
        }
        return redirect(route('routes.sort', $route))
            ->with('success', 'Orden guardado');
    }

    public function routeCobrador()
    {
        $currentuser = Auth::user();

        $routes = Route::query()
            ->where('cobrador_id', '=', $currentuser->id)->get();
        return Inertia::render('Private/Route/RouteCobrador', [
            'availableRoutes' => $routes,
        ]);
    }
    /**
     *  the specified resource from storage.
     *
     * @param  \App\Models\Route  $route
     */
    public function consultPendingLoansInRoute(Route  $route)
    {

        $currentuser = Auth::user();

        $routeVerify = Route::query()
            ->where('cobrador_id', '=', $currentuser->id)
            ->where('id', '=', $route->id)
            ->first();
        if(!$routeVerify){
            return redirect(route('dashboard'))->with('success', 'No es tu ruta');
        }

        $pagosPendientesPorRuta = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<=',Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->where('loands.route_id', '=',$route->id)
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id',
                'loands.route_id as route_id',
            ]);

        $prestamosPendientesPorRuta = 0;
        if( count($pagosPendientesPorRuta) > 0){
            /*$prestamosPendientesPorRuta = $pagosPendientesPorRuta->unique('loand_id')
                ->pluck('loand_id')
                ->count();*/

            $prestamosPendientesRutaIds = $pagosPendientesPorRuta
                ->unique('loand_id')
                ->pluck('loand_id');

            $prestamosPendientesPorRuta = Loand::query()
                ->whereIn('id', $prestamosPendientesRutaIds)
                ->where('state','=', "ACTIVO")
                ->where('visited','=', false)
                ->orderBy('order','ASC')
                ->count();
        }

        $existHistory = RouteCheckHistory::where('date','=', Carbon::now()->format('Y-m-d'))
            ->where('route_id','=', $route->id)
            ->exists();

        if($prestamosPendientesPorRuta === 0 && $route->state !=="COMPLETADA" && $existHistory === false){
            $route->update(['state' => 'COMPLETADA']);
        }

        return response()->json(['pendingLoansInRoute'=>$prestamosPendientesPorRuta]);
    }

    /**
     *  the specified resource from storage.
     *
     * @param  \App\Models\Route  $route
     */
    public function collect(Route  $route)
    {
        $currentuser = Auth::user();

        $routeVerify = Route::query()
            ->where('cobrador_id', '=', $currentuser->id)
            ->where('id', '=', $route->id)
            ->first();
        if(!$routeVerify){
            return redirect(route('dashboard'))
                ->with('success', 'No es tu ruta');
        }

        $pagosPendientesPorRuta = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<=',Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->where('loands.route_id', '=',$route->id)
            ->get([
                'payments.id',
                'loands.id as loand_id',
                /*'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.route_id as route_id',*/
            ]);

        $loans = [];
        if( count($pagosPendientesPorRuta) > 0) {
            $prestamosPendientesRutaIds = $pagosPendientesPorRuta
                ->unique('loand_id')
                ->pluck('loand_id');

            $loans = Loand::query()
                ->whereIn('id', $prestamosPendientesRutaIds)
                ->where('state', '=', "ACTIVO")
                ->where('visited', '=', false)
                ->orderBy('order', 'ASC')
                ->get();

            foreach ($loans as $loan) {
                $business = $loan->business_name ? $loan->business_name . " - " : "";
                $loan->customer_name = $business . " " . $loan->customer->firstName . " " . $loan->customer->lastName;
            }
        }

        if(count($loans)>0){
            $route->update(['state'=> 'EN TRANSITO']);
        }else{
            $route->update(['state'=> 'COMPLETADA']);
        }

        return Inertia::render('Private/Route/RouteCollect', [
            'routeParam' => $route,
            'loans' => $loans,
            'loansCount' => count($loans),
        ]);
    }

    /**
     * Display the specified resource.
     * @param  \App\Models\Route  $route
     * @param  \App\Models\Loand  $loand
     */
    public function checkRouteLoand(Route  $route, Loand  $loand)
    {
        $loand->update(['visited'=>true]);
        return redirect(route('routes.collect', $route))
            ->with('success', 'El prestamo ha sido visitado.');
    }


    /**
     *  the specified resource from storage.
     *
     * @param  \App\Models\Route  $route
     */
    public function collectTomorrow(Route  $route)
    {
        $pagosPendientesPorRuta = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<=',Carbon::now()->addDay(1)->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->where('loands.route_id', '=',$route->id)
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id',
                'loands.route_id as route_id',
            ]);

        $loans = [];

        if($pagosPendientesPorRuta->count() > 0){
            $prestamosPendientesRutaIds = $pagosPendientesPorRuta
                ->unique('loand_id')
                ->pluck('loand_id');

            $loans = Loand::query()
                ->whereIn('id', $prestamosPendientesRutaIds)
                ->where('state','=', "ACTIVO")
                ->where('visited','=', false)
                ->orderBy('order','ASC')
                ->get();

            foreach ($loans  as $loan ){
                $business = $loan->business_name? $loan->business_name." - " : "";
                $loan->customer_name = $business." ".$loan->customer->firstName." ".$loan->customer->lastName;
            }

            if($route->state === 'CUADRADA'){
                $route->update(['state'=> 'SIN INICIAR']);
            }
        }

        return response()->json([
            'loans' => $loans,
            'loansCount' => count($loans),
        ]);
    }
}
