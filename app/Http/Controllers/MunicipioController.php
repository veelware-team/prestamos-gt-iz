<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Loand;
use App\Models\Municipio;
use App\Models\Sector;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MunicipioController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:municipios.view')->only('view');
        $this->middleware('can:municipios.update')->only('update');
    }

    /**
     * Display a view of the resource.
     *
     * @return \Inertia\Response
     */
    public function view ()
    {
        return Inertia::render('Private/Settings/Municipios');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $municipios = Municipio::whereHas('departamento', function (Builder $query) {
            $query->where('active', '=', true);
        })
            ->when($request->has('active'), function (Builder $query) use ($request) {
                $query->where('active','=', $request->active);
            })
            ->get()->map(function ($municipioTemp){
                $existLoans = Loand::query()
                    ->where('municipio_id','=', $municipioTemp->id)
                    ->count();
                $existCustomer = Customer::query()
                    ->where('municipio_id','=', $municipioTemp->id)
                    ->count();
                return [
                    'id'=>$municipioTemp->id,
                    'name'=>$municipioTemp->name,
                    'active'=>$municipioTemp->active,
                    'departamento'=>$municipioTemp->departamento->name,
                    'departamento_id'=>$municipioTemp->departamento->id,
                    'exist_loans'=>$existLoans,
                    'exist_customers'=>$existCustomer,
                ];
            });
        return response()->json(['municipios'=>$municipios]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Municipio  $municipio
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Municipio $municipio)
    {
        $active = false;
        if($request->active == 0){
            $active = true;
        }

        $municipio->update(['active' => $active]);
        if(!$active){
            if($municipio->sectores()->where('active','=',true)->count() > 0){
                Sector::query()->where('active', '=', true)->update(['active' => false]);
            }
        }

        return response()->json(['message'=>'El departamento ha sido actualizado.']);
    }

}
