<?php

namespace App\Http\Controllers;

use App\Models\Loand;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ReportController extends Controller
{
    public function __construct()
    {

        $this->middleware('can:reports.menu')->only('menu');

        $this->middleware('can:reports.approvedLoans')->only(['approvedLoans','approvedLoansList']);
        $this->middleware('can:reports.arrearsFees')->only(['arrearsFees','arrearsFeesList']);
        $this->middleware('can:reports.paidLoans')->only(['paidLoans','paidLoansList']);
        $this->middleware('can:reports.typeLoans')->only(['typeLoans','typeLoansList']);

    }
    public function menu ()
    {
        return Inertia::render('Private/Report/ReportMenu');
    }

    public function activeLoans ()
    {
        return Inertia::render('Private/Report/ReportActiveLoans');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activeLoansList(Request $request){

        $loans = Loand::query()
            ->where('state', "ACTIVO")
            ->orderBy('created_at','DESC')
            ->get()->map(function ($object){

                //lista cuotas pagadas
                $payments_paid_out = $object->payments()
                    ->where('paid_out','=',true)
                    ->get();
                //capital + interes
                $montoPrestado = $object->total_amount;

                $montoCobrado = $payments_paid_out->sum('total_payment');
                $capitalCobrado = $payments_paid_out->sum('capital_payment');
                $interesCobrado = $payments_paid_out->sum('interest_payment');
                $moraCobrada =  $payments_paid_out->sum('late_fee');

                $montoxCobrar =  $montoPrestado - $montoCobrado;
                $capitalxCobrar = $object->borrowed_amount - $capitalCobrado;
                $interesxCobrar = $object->interest_amount - $interesCobrado;

                $business = $object->business_name? $object->business_name." - " : "";
                return [
                    'id'=>$object->id,
                    'created_at'=>$object->created_at->format('d-m-Y'),
                    'correlative'=>$object->correlative,
                    'customer'=> $business." ".$object->customer->firstName." ".$object->customer->lastName,
                    'type'=>$object->type,
                    'montoPrestado' => $montoPrestado,
                    'montoCobrado' => $montoCobrado,
                    'montoxCobrar' => $montoxCobrar,
                    'capitalCobrado' => $capitalCobrado,
                    'interesCobrado' => $interesCobrado,
                    'moraCobrada' => $moraCobrada,
                    'capitalxCobrar' => $capitalxCobrar,
                    'interesxCobrar' => $interesxCobrar,
                ];
            });
        return response()->json(['items'=>$loans]);
    }

    public function approvedLoans ()
    {
        return Inertia::render('Private/Report/ReportApprovedLoans');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function approvedLoansList (Request  $request)
    {
        $loans = Loand::query()
            ->whereBetween('confirmed_at', [$request->dateFrom, $request->dateTo])
            ->orderBy('confirmed_at','DESC')
            ->get()->map(function ($object){

                $payments_paid_out = $object->payments()
                    ->where('paid_out','=',true)
                    ->get();

                $count_paid_out = count($payments_paid_out);
                $collected_amount = $payments_paid_out->sum('total_payment');

                $business = $object->business_name? $object->business_name." - " : "";
                return [
                    'id'=>$object->id,
                    'created_at'=>$object->created_at->format('d-m-Y'),
                    'confirmed_at'=>$object->created_at->format('d-m-Y'),
                    'correlative'=>$object->correlative,
                    'customer'=> $business." ".$object->customer->firstName." ".$object->customer->lastName,
                    'fixed_fee'=>$object->fixed_fee,
                    'type'=>$object->type,
                    'dues'=>$object->dues,
                    'dues_paid_out'=> $count_paid_out,
                    'interest_percentage'=>$object->interest_percentage."%",
                    'borrowed_amount'=>$object->borrowed_amount,
                    'total_amount'=>$object->total_amount,
                    'interest_amount'=>$object->interest_amount,
                    'collected_amount'=>$collected_amount,
                    'state'=>$object->state,
                ];
            });
        return response()->json(['items'=>$loans]);
    }

    public function arrearsFees ()
    {
        return Inertia::render('Private/Report/ReportArrearsFees');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function arrearsFeesList ()
    {
        $pagosAtrasados = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->join('customers', 'loands.customer_id', '=', 'customers.id')
            ->where('payments.estimated_payment_date', '<',Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->where('loands.state', '=',"ACTIVO")
            ->get([
                'payments.id',
                'payments.estimated_payment_date',
                'payments.total_payment',
                'loands.id as loand_id',
                'loands.correlative as loan_correlative',
                'loands.type as loan_type',
                'loands.dues as loan_dues',
                'loands.late_fee as loan_late_fee',
                'loands.business_name as loan_business_name',
                'customers.firstName as customer_firstName',
                'customers.lastName as customer_lastName',
            ]);

        if(count($pagosAtrasados)>0){
            foreach ($pagosAtrasados as $item){
                $business = $item->loan_business_name? $item->loan_business_name." - " : "";
                $item->customer = $business." ".$item->customer_firstName." ".$item->customer_lastName;

                $mora = $item->loan_late_fee ? $item->loan_late_fee : 0;
                $item->total_amount = $item->total_payment + $mora;

                $numeroDeCuota = Payment::query()
                        ->where('estimated_payment_date','<', $item->estimated_payment_date)
                        ->where('loand_id','=', $item->loand_id)
                        ->count()+1;
                $item->number_late_fee = $numeroDeCuota."/".$item->loan_dues;
            }
        }

        return response()->json(['items'=>$pagosAtrasados]);
    }

    public function paidLoans ()
    {
        return Inertia::render('Private/Report/ReportPaidLoans');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paidLoansList (Request  $request)
    {

        $dateFrom = Carbon::createFromFormat('Y-m-d',  $request->dateFrom)->startOfDay();
        $dateTo = Carbon::createFromFormat('Y-m-d',  $request->dateTo)->endOfDay();

        $loans = Loand::query()
            ->whereBetween('completed_at', [
                $dateFrom,
                $dateTo,
            ])
            ->where('state', "PAGADO")
            ->orderBy('completed_at','DESC')
            ->get()->map(function ($object){

                $payments_paid_out = $object->payments()
                    ->where('paid_out','=',true)
                    ->get();

                $collected_amount = $payments_paid_out->sum('total_payment');

                $late_fee_amount = $payments_paid_out->sum('late_fee');
                $gain = $collected_amount - $object->borrowed_amount;

                $business = $object->business_name? $object->business_name." - " : "";
                return [
                    'id'=>$object->id,
                    'created_at'=>$object->created_at->format('d-m-Y'),
                    'completed_at'=>$object->completed_at->format('d-m-Y'),
                    'correlative'=>$object->correlative,
                    'customer'=> $business." ".$object->customer->firstName." ".$object->customer->lastName,
                    'type'=>$object->type,
                    'borrowed_amount'=>$object->borrowed_amount,
                    'interest_amount'=>$object->interest_amount,
                    'total_amount'=>$object->total_amount,
                    'late_fee_amount'=>$late_fee_amount,
                    'collected_amount'=>$collected_amount,
                    'gain'=>$gain,
                ];
            });

        $daily_loans = $loans->filter(function ($item) {
            return data_get($item, 'type') == "DIARIO";
        });
        $count_daily_loans = count($daily_loans);
        $total_borrowed_daily_loans = 0;
        $total_gain_daily_loans = 0;
        if ($count_daily_loans > 0 ){
            $total_borrowed_daily_loans = $daily_loans->sum('borrowed_amount');
            $total_gain_daily_loans = $daily_loans->sum('gain');;
        }

        $weekly_loans = $loans->filter(function ($item) {
            return data_get($item, 'type') == "SEMANAL";
        });
        $count_weekly_loans = count($weekly_loans);
        $total_borrowed_weekly_loans = 0;
        $total_gain_weekly_loans = 0;
        if ($count_weekly_loans > 0 ){
            $total_borrowed_weekly_loans = $weekly_loans->sum('borrowed_amount');
            $total_gain_weekly_loans = $weekly_loans->sum('gain');;
        }

        $biweekly_loans = $loans->filter(function ($item) {
            return data_get($item, 'type') == "QUINCENAL";
        });
        $count_biweekly_loans = count($biweekly_loans);
        $total_borrowed_biweekly_loans = 0;
        $total_gain_biweekly_loans = 0;
        if ($count_biweekly_loans > 0 ){
            $total_borrowed_biweekly_loans = $biweekly_loans->sum('borrowed_amount');
            $total_gain_biweekly_loans = $biweekly_loans->sum('gain');;
        }

        $monthly_loans = $loans->filter(function ($item) {
            return data_get($item, 'type') == "MENSUAL";
        });
        $count_monthly_loans = count($monthly_loans);
        $total_borrowed_monthly_loans = 0;
        $total_gain_monthly_loans = 0;
        if ($count_monthly_loans > 0 ){
            $total_borrowed_monthly_loans = $monthly_loans->sum('borrowed_amount');
            $total_gain_monthly_loans = $monthly_loans->sum('gain');;
        }


        $total_loans = count($loans);
        $total_borrowed =  $loans->sum('borrowed_amount');
        $total_gain =  $loans->sum('gain');
        return response()->json([
            'items'=>$loans,

            'total_loans'=>$total_loans,
            'total_borrowed'=>$total_borrowed,
            'total_gain'=>$total_gain,

            'count_daily_loans'=>$count_daily_loans,
            'total_borrowed_daily_loans'=>$total_borrowed_daily_loans,
            'total_gain_daily_loans'=>$total_gain_daily_loans,

            'count_weekly_loans'=>$count_weekly_loans,
            'total_borrowed_weekly_loans'=>$total_borrowed_weekly_loans,
            'total_gain_weekly_loans'=>$total_gain_weekly_loans,

            'count_biweekly_loans'=>$count_biweekly_loans,
            'total_borrowed_biweekly_loans'=>$total_borrowed_biweekly_loans,
            'total_gain_biweekly_loans'=>$total_gain_biweekly_loans,

            'count_monthly_loans'=>$count_monthly_loans,
            'total_borrowed_monthly_loans'=>$total_borrowed_monthly_loans,
            'total_gain_monthly_loans'=>$total_gain_monthly_loans,
        ]);
    }

    public function typeLoans ()
    {
        return Inertia::render('Private/Report/ReportTypeLoans');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function typeLoansList (Request  $request)
    {

        $dateFrom = Carbon::createFromFormat('Y-m-d',  $request->dateFrom)->startOfDay();
        $dateTo = Carbon::createFromFormat('Y-m-d',  $request->dateTo)->endOfDay();

        $loans = Loand::query()
            ->whereBetween('created_at', [
                $dateFrom,
                $dateTo,
            ])
            ->where(function($q){
                $q->where('state', '=', 'APROBADO')
                    ->orWhere('state', '=','ACTIVO')
                    ->orWhere('state', '=','PAGADO');
            })
            ->orderBy('created_at','DESC')
            ->get()->map(function ($object){
                $payments_paid_out = $object->payments()
                    ->where('paid_out','=',true)
                    ->get();

                $collected_amount = $payments_paid_out->sum('total_payment');

                $business = $object->business_name? $object->business_name." - " : "";
                return [
                    'id'=>$object->id,
                    'created_at'=>$object->created_at->format('d-m-Y'),
                    'first_payment_date'=> $object->first_payment_date ? $object->first_payment_date->format('d-m-Y') : "",
                    'correlative'=>$object->correlative,
                    'customer'=> $business." ".$object->customer->firstName." ".$object->customer->lastName,
                    'type'=>$object->type,
                    'state'=>$object->state,
                    'borrowed_amount'=>$object->borrowed_amount,
                    'interest_amount'=>$object->interest_amount,
                    'total_amount'=>$object->total_amount,
                    'collected_amount'=>$collected_amount,
                ];
            });

        $daily_loans = $loans->filter(function ($item) {
            return data_get($item, 'type') == "DIARIO";
        });
        $count_daily_loans = count($daily_loans);
        $total_borrowed_daily_loans = 0;
        $total_gain_daily_loans = 0;
        if ($count_daily_loans > 0 ){
            $total_borrowed_daily_loans = $daily_loans->sum('borrowed_amount');
            $total_gain_daily_loans = $daily_loans->sum('collected_amount');
        }

        $weekly_loans = $loans->filter(function ($item) {
            return data_get($item, 'type') == "SEMANAL";
        });
        $count_weekly_loans = count($weekly_loans);
        $total_borrowed_weekly_loans = 0;
        $total_gain_weekly_loans = 0;
        if ($count_weekly_loans > 0 ){
            $total_borrowed_weekly_loans = $weekly_loans->sum('borrowed_amount');
            $total_gain_weekly_loans = $weekly_loans->sum('collected_amount');
        }

        $biweekly_loans = $loans->filter(function ($item) {
            return data_get($item, 'type') == "QUINCENAL";
        });
        $count_biweekly_loans = count($biweekly_loans);
        $total_borrowed_biweekly_loans = 0;
        $total_gain_biweekly_loans = 0;
        if ($count_biweekly_loans > 0 ){
            $total_borrowed_biweekly_loans = $biweekly_loans->sum('borrowed_amount');
            $total_gain_biweekly_loans = $biweekly_loans->sum('collected_amount');
        }

        $monthly_loans = $loans->filter(function ($item) {
            return data_get($item, 'type') == "MENSUAL";
        });
        $count_monthly_loans = count($monthly_loans);
        $total_borrowed_monthly_loans = 0;
        $total_gain_monthly_loans = 0;
        if ($count_monthly_loans > 0 ){
            $total_borrowed_monthly_loans = $monthly_loans->sum('borrowed_amount');
            $total_gain_monthly_loans = $monthly_loans->sum('collected_amount');
        }


        $total_loans = count($loans);
        $total_borrowed =  $loans->sum('borrowed_amount');
        $total_gain =  $loans->sum('collected_amount');
        return response()->json([
            'items'=>$loans,

            'total_loans'=>$total_loans,
            'total_borrowed'=>$total_borrowed,
            'total_gain'=>$total_gain,

            'count_daily_loans'=>$count_daily_loans,
            'total_borrowed_daily_loans'=>$total_borrowed_daily_loans,
            'total_gain_daily_loans'=>$total_gain_daily_loans,

            'count_weekly_loans'=>$count_weekly_loans,
            'total_borrowed_weekly_loans'=>$total_borrowed_weekly_loans,
            'total_gain_weekly_loans'=>$total_gain_weekly_loans,

            'count_biweekly_loans'=>$count_biweekly_loans,
            'total_borrowed_biweekly_loans'=>$total_borrowed_biweekly_loans,
            'total_gain_biweekly_loans'=>$total_gain_biweekly_loans,

            'count_monthly_loans'=>$count_monthly_loans,
            'total_borrowed_monthly_loans'=>$total_borrowed_monthly_loans,
            'total_gain_monthly_loans'=>$total_gain_monthly_loans,
        ]);
    }
}
