<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Loand;
use App\Models\Sector;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SectorController extends Controller
{
    public function __construct()
    {

         $this->middleware('can:sectores.view')->only('view');
         $this->middleware('can:sectores.new')->only('store');
         $this->middleware('can:sectores.update')->only('update');
         $this->middleware('can:sectores.destroy')->only('destroy');

    }

    public function view ()
    {
        return Inertia::render('Private/Settings/Sectores');
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request  $request)
    {
        $sectores = Sector::query()
            ->when($request->has('active'), function (Builder $query) use ($request) {
                $query->where('active','=', $request->active);
            })
            ->get()->map(function ($sectorTemp){
                $existLoans = Loand::query()
                    ->where('sector_id','=', $sectorTemp->id)
                    ->count();
                $existCustomer = Customer::query()
                    ->where('sector_id','=', $sectorTemp->id)
                    ->count();
                return [

                    'id'=>$sectorTemp->id,
                    'name'=>$sectorTemp->name,
                    'active'=>$sectorTemp->active,
                    'departamento'=>$sectorTemp->departamento->name,
                    'departamento_id'=>$sectorTemp->departamento->id,
                    'municipio'=>$sectorTemp->municipio->name,
                    'municipio_id'=>$sectorTemp->municipio->id,
                    'municipio_active'=>$sectorTemp->municipio->active,
                    'exist_loans'=>$existLoans,
                    'exist_customers'=>$existCustomer,
                ];
            });
        return response()->json(['sectores'=>$sectores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $sector = Sector::create($request->only([
            'name',
            'departamento_id',
            'municipio_id',
            'active'
        ]));
        return response()->json(['message'=>'El sector ha sido creado.']);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sector  $sector
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Sector $sector)
    {
       if($request->handleState){
           $sector->update(['active' => !$request->active]);
           return response()->json(['message'=>'El sector ha sido actualizado.']);
       }

       $sector->update($request->only(['name','departamento_id','municipio_id','active']));
       return response()->json(['message'=>'El sector ha sido actualizado.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sector  $sector
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Sector $sector)
    {
        $type= "success";
        $message="El sector ha sido eliminado";

        $existLoans = Loand::query()
            ->where('sector_id','=', $sector->id)
            ->count();
        $existCustomers = Customer::query()
            ->where('sector_id','=', $sector->id)
            ->count();
        if($existLoans>0 || $existCustomers>0){
            $type= "warning";
            $message="No puedes eliminar el sector ya que hay prestamos o clientes con el.";
        }else{
            $sector->delete();
        }
        return response()->json([
            'type'=> $type,
            'message'=> $message,
        ]);
    }
}
