<?php

namespace App\Http\Controllers;

use App\Models\DailySpend;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class DailySpendController extends Controller
{

    public function __construct()
    {

        $this->middleware('can:dailySpends.view')->only(['view','index']);
        $this->middleware('can:dailySpends.totalDaily')->only('getTotalDaily');
        $this->middleware('can:dailySpends.new')->only('store');
        $this->middleware('can:dailySpends.cancel')->only('cancel');

    }

    public function view ()
    {
        return Inertia::render('Private/DailySpend/DailySpendIndex');
    }


    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request  $request)
    {
        $items = DailySpend::query()
            ->whereBetween('date', [$request->dateFrom, $request->dateTo])
            ->orderBy('correlative','DESC')
            ->get();
        foreach ($items as $item ){
            $creator =  $item->creator()->select(['firstName','lastName', 'username'])->first();
            if($creator){
                $creatorfirstName = explode(" ", $creator->firstName )[0];
                $creatorlastName = explode(" ", $creator->lastName )[0];
                $item["created_by_name"] = $creator->username." ( ".$creatorfirstName." ".$creatorlastName.")";
            }
            if($item->nullified_at){
                $nullifier =  $item->nullifier()->select(['firstName','lastName', 'username'])->first();
                $nullifierfirstName = explode(" ", $nullifier->firstName )[0];
                $nullifierlastName = explode(" ", $nullifier->lastName )[0];
                $item["nullified_by_name"] = $nullifier->username." ( ".$nullifierfirstName." ".$nullifierlastName.")";
            }
            $item["petty_cash_correlative"] = $item->pettyCashHistory->correlative;
            $item["petty_cash_state"] = $item->pettyCashHistory->state;
        }
        return response()->json(['items'=>$items]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->merge([
            'correlative' => DailySpend::nextCorrelative(),
            'created_by' =>  Auth::id(),
            'date' =>  Carbon::now(),
            'state' =>  "REGISTRADO",
        ]);
        $dailySpend = DailySpend::create($request->all());
        return redirect(route('pettyCashHistories.dailyView'))
            ->with('success', 'Retiro registrado');
    }


    public function getTotalDaily(){
        $totalDailySpend = DailySpend::query()
            ->where('date', '=',Carbon::now()->format('Y-m-d'))
            ->where('state','=', "REGISTRADO")
            ->sum('amount');
        return response()->json(['totalDailySpend'=> $totalDailySpend]);
    }

    /**
     * Cancel the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DailyIncome  $dailySpend
     */
    public function cancel(Request $request, DailySpend $dailySpend)
    {
        $request->merge([
            'nullified_by' =>  Auth::id(),
            'nullified_at' =>  Carbon::now(),
            'state' =>  "ANULADO",
        ]);
        $dailySpend->update($request->all());
        return redirect(route('dailySpends.view'))
            ->with('success', 'Retiro anulado');
    }
}
