<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Loand;
use App\Models\Payment;
use App\Models\Route;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function view ()
    {
        return Inertia::render('Private/Dashboard');
    }

    public function newCustomers()
    {
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();
        $total = Customer::query()
            ->whereBetween('created_at', [$start, $end])
            ->count();

        if($total<10) {
            $total = str_pad($total, 2, "0", STR_PAD_LEFT);
        }
        return response()->json(['total'=>$total]);
    }

    public function activeCustomers()
    {
        $total = Customer::query()
            ->where('active', '=', true)
            ->count();

        if($total<10) {
            $total = str_pad($total, 2, "0", STR_PAD_LEFT);
        }
        return response()->json(['total'=>$total]);
    }

    public function totalCustomers()
    {
        $total = Customer::all()->count();

        if($total<10) {
            $total = str_pad($total, 2, "0", STR_PAD_LEFT);
        }
        return response()->json(['total'=>$total]);
    }

    public function loanToApprove()
    {
        $total = Loand::query()
            ->where('state', '=', 'PENDIENTE')
            ->count();

        if($total<10) {
            $total = str_pad($total, 2, "0", STR_PAD_LEFT);
        }
        return response()->json(['total'=>$total]);
    }

    public function activeLoans()
    {
        $total = Loand::query()
            ->where('state', '=', 'ACTIVO')
            ->count();

        if($total<10) {
            $total = str_pad($total, 2, "0", STR_PAD_LEFT);
        }
        return response()->json(['total'=>$total]);
    }

    public function dailyRouteAmount()
    {
        $pagosDiarios = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('estimated_payment_date', '=', Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);


        $totalDiario = 0;
        foreach ($pagosDiarios as $item){
            $montoPrestamo = $item->capital_payment + $item->interest_payment;
            $totalDiario += $montoPrestamo;
        }

        $pagosDiariosPagados = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '=', Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',true)
            ->where('payments.payment_date', '=',Carbon::now()->format('Y-m-d'))
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);


        foreach ($pagosDiariosPagados as $item){
            $montoPrestamo = $item->capital_payment + $item->interest_payment;
            $totalDiario += $montoPrestamo;
        }


        //dd($totalDiario);

        // pagos atrasados
        $pagosAtrasado = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<',Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->where('loands.state', '=',"ACTIVO")
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);

        //dd($pagosAtrasado->count());

        $totalPagosAtrasado = 0;
        foreach ($pagosAtrasado as $item){
            $mora = $item->loand_late_fee ? $item->loand_late_fee : 0 ;
            $montoPrestamo = $item->capital_payment + $item->interest_payment + $mora;
            $totalPagosAtrasado += $montoPrestamo;
        }

        //dd($totalPagosAtrasado);
        // pagos atrasados pagados
        $pagosAtrasadoPagado = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<',Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',true)
            ->where('payments.payment_date', '=',Carbon::now()->format('Y-m-d'))
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee'
            ]);

        $totalDiarioAtrasadoPagado = 0;
        foreach ($pagosAtrasadoPagado as $item){
            $mora = $item->loand_late_fee ? $item->loand_late_fee : 0 ;
            $montoPrestamo = $item->capital_payment + $item->interest_payment + $mora;
            $totalDiarioAtrasadoPagado += $montoPrestamo;
        }

        //dd($totalDiarioAtrasadoPagado);
        $total_sum = $totalDiario + $totalPagosAtrasado + $totalDiarioAtrasadoPagado;
        return response()->json(['total'=>$total_sum]);
    }


    public function dailyRouteAmountCharged()
    {
        $total = Payment::query()
            ->where('payment_date', '=',Carbon::now()->format('Y-m-d'))
            ->where('state','=', "PAGADO")
            ->sum('total_payment');
        return response()->json(['total'=>$total]);
    }


    public function loansInArrears()
    {
        // pagos atrasados
        $pagosAtrasado = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<',Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->where('loands.state', '=',"ACTIVO")
            ->get([
                'payments.id',
                'loands.id as loand_id'
            ]);

        $totalLoansInArrears = $pagosAtrasado->groupBy('loand_id')->map(function ($row) {
            return $row->count();
        })->count();

        if($totalLoansInArrears<10) {
            $totalLoansInArrears = str_pad($totalLoansInArrears, 2, "0", STR_PAD_LEFT);
        }

        return response()->json(['total'=>$totalLoansInArrears]);

    }


    public function testQuerys(){

        $pagosDiarios = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('estimated_payment_date', '=', Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);


        $totalDiario = 0;
        foreach ($pagosDiarios as $item){
            $montoPrestamo = $item->capital_payment + $item->interest_payment;
            $totalDiario += $montoPrestamo;
        }

        $pagosDiariosPagados = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '=', Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',true)
            ->where('payments.payment_date', '=',Carbon::now()->format('Y-m-d'))
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);


        foreach ($pagosDiariosPagados as $item){
            $montoPrestamo = $item->capital_payment + $item->interest_payment;
            $totalDiario += $montoPrestamo;
        }


        //dd($totalDiario);

        // pagos atrasados
        $pagosAtrasado = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<',Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',false)
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee',
                'loands.id as loand_id'
            ]);

        //dd($pagosAtrasado->count());

        $totalPagosAtrasado = 0;
        foreach ($pagosAtrasado as $item){
            $mora = $item->loand_late_fee ? $item->loand_late_fee : 0 ;
            $montoPrestamo = $item->capital_payment + $item->interest_payment + $mora;
            $totalPagosAtrasado += $montoPrestamo;
        }

        //dd($totalPagosAtrasado);
        // pagos atrasados pagados
        $pagosAtrasadoPagado = Payment::join('loands', 'payments.loand_id', '=', 'loands.id')
            ->where('payments.estimated_payment_date', '<',Carbon::now()->format('Y-m-d'))
            ->where('payments.paid_out', '=',true)
            ->where('payments.payment_date', '=',Carbon::now()->format('Y-m-d'))
            ->get([
                'payments.id',
                'payments.capital_payment',
                'payments.interest_payment',
                'loands.late_fee as loand_late_fee'
            ]);

        $totalDiarioAtrasadoPagado = 0;
        foreach ($pagosAtrasadoPagado as $item){
            $mora = $item->loand_late_fee ? $item->loand_late_fee : 0 ;
            $montoPrestamo = $item->capital_payment + $item->interest_payment + $mora;
            $totalDiarioAtrasadoPagado += $montoPrestamo;
        }

        //dd($totalDiarioAtrasadoPagado);
        $total_sum = $totalDiario + $totalPagosAtrasado + $totalDiarioAtrasadoPagado;


        return response()->json(['total_sum'=>$total_sum]);

    }
}
