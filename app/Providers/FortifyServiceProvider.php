<?php

namespace App\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Models\User;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;
use Laravel\Fortify\Fortify;


class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
        Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);
         RateLimiter::for('two-factor', function (Request $request) {
             return Limit::perMinute(5)->by($request->session()->get('login.id'));
         });*/

        RateLimiter::for('login', function (Request $request) {
            return Limit::perMinute(5)->by($request->email.$request->ip());
        });

        /* Defincion de vistas*/

        /*Login*/

        Fortify::loginView( function (){
            return Inertia::render('Public/Auth/Login');
        });

        /* Login personalizado*/

        Fortify::authenticateUsing(function (Request $request) {
            $user = User::where('username', $request->username)->first();
            if ($user && Hash::check($request->password, $user->password)) {
                Auth::logoutOtherDevices($request->password);
                return $user;
            }
        });

        /* -Registrar */

        /*Fortify::registerView(function () {
            return view('auth.register');
        });*/

        /* Solicitud para restablecer contraseña */

        /*Fortify::requestPasswordResetLinkView(function () {
            return view('auth.forgot-password');
        });*/

        /* Vista para ingresar la contraseña */

        /* Fortify::resetPasswordView(function ($request) {
            return view('auth.reset-password', ['request' => $request]);
        });*/

        /* Vista para verificar email */

        /*Fortify::verifyEmailView(function () {
            return view('auth.verify-email');
        });*/

        /* Restablecer contraseña personalizado - meter datos*/

        /*Fortify::requestPasswordResetLinkView(function () {
            return view('auth.forgot-password');
        });*/
    }
}
