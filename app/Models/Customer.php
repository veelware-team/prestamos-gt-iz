<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $dates = ['created_at', 'deactivated_at'];

    public function departamento(){
        return $this->hasOne(
            Departamento::class,
            'id',
            'departamento_id'
        );
    }

    public function municipio()
    {
        return $this->hasOne(
            Municipio::class,
            'id',
            'municipio_id'
        );
    }

    public function sector()
    {
        return $this->hasOne(
            Sector::class,
            'id',
            'sector_id'
        );
    }

    public function creator(){
        return $this->hasOne(
            User::class,
            'id',
            'created_by'
        );
    }

    public function deactivator(){
        return $this->hasOne(
            User::class,
            'id',
            'deactivated_by'
        );
    }

    public function image(){
        return $this->morphOne(Image::class,'modelo');
    }

    public function loands()
    {
        return $this->hasMany(Loand::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y h:i a');
    }

    public static function nextCorrelative()
    {
        $year = Carbon::now()->format('y');
        $lastCustomer = Customer::query()->where('correlative', 'like', '%'.$year.'%')->orderBy('correlative','DESC')->first();
        $number = 1;
        if($lastCustomer){
            $numberLastCustomer = explode("-",$lastCustomer->correlative)[1];
            $number = (int)$numberLastCustomer+1;
        }
        return 'CL'.$year.'-'.$number;
    }

}
