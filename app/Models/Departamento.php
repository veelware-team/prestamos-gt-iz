<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function municipios(){
        return $this->hasMany(Municipio::class);
    }

    public function sectores(){
        return $this->hasMany(Sector::class);
    }

    public function route(){
        return $this->belongsTo(Route::class);
    }
}
