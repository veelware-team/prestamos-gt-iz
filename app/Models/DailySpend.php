<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailySpend extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $dates = [
        'nullified_at',
        'date',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y');
    }

    public function pettyCashHistory()
    {
        return $this->belongsTo(PettyCashHistory::class);
    }

    public function creator(){
        return $this->hasOne(
            User::class,
            'id',
            'created_by'
        );
    }

    public function nullifier(){
        return $this->hasOne(
            User::class,
            'id',
            'nullified_by'
        );
    }


    public static function nextCorrelative(){
        //RCC-21-05-14-001
        $dateCorrelative = Carbon::now()->format('y-m-d');
        $counter = 1;
        $lastDailyIncome = DailySpend::query()->where('correlative', 'like', 'RCC-'.$dateCorrelative.'%')
            ->orderBy('correlative','DESC')
            ->first();
        if($lastDailyIncome){
            $counterLastDailyIncome = explode("-",$lastDailyIncome->correlative)[4];
            $counter = (int)$counterLastDailyIncome+1;
        }
        return "RCC-".$dateCorrelative."-".str_pad($counter, 3, "0", STR_PAD_LEFT);
    }
}
