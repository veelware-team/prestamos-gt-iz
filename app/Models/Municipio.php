<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }

    public function sectors(){
        return $this->hasMany(Sector::class);
    }

    public function routes()
    {
        return $this->belongsToMany(Route::class);
    }

}
