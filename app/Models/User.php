<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable , HasRoles;

    protected $fillable = [
        'username',
        'firstName',
        'lastName',
        'email',
        'password',
        'identificationNumber',
        'address',
        'phoneOne',
        'phoneTwo',
        'active',
        'personal_password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function image(){
        return $this->morphOne(Image::class,'modelo');
    }

    public function initials(){
        $firstName = explode(" ", $this->firstName );
        $lastName = explode(" ", $this->lastName );
        $initials = mb_substr($firstName[0],0,1) . mb_substr($lastName[0],0,1);
        return strtoupper($initials);
    }


    public function routes(){
        return $this->hasMany(Route::class,'cobrador_id','id');
    }
}
