<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loand extends Model
{
    use HasFactory;
    protected $guarded= [];

    protected $dates = [
        'created_at',
        'confirmed_at' ,
        'nullified_at',
        'rejected_at',
        'completed_at',
        'first_payment_date',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function creator(){
        return $this->hasOne(
            User::class,
            'id',
            'created_by'
        );
    }

    public function confirmer(){
        return $this->hasOne(
            User::class,
            'id',
            'confirmed_by'
        );
    }

    public function nullifier(){
        return $this->hasOne(
            User::class,
            'id',
            'nullified_by'
        );
    }

    public function rejector(){
        return $this->hasOne(
            User::class,
            'id',
            'rejected_by'
        );
    }

    public function departamento(){
        return $this->hasOne(
            Departamento::class,
            'id',
            'departamento_id'
        );
    }

    public function municipio()
    {
        return $this->hasOne(
            Municipio::class,
            'id',
            'municipio_id'
        );
    }

    public function sector()
    {
        return $this->hasOne(
            Sector::class,
            'id',
            'sector_id'
        );
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public static function keyTypeLoand($type)
    {
        $number = "";
        switch ($type){
            case "DIARIO":
                $number = 1;
                break;
            case "SEMANAL":
                $number = 2;
                break;
            case "QUINCENAL":
                $number = 3;
                break;
            case "MENSUAL":
                $number = 4;
                break;
            default:
                break;
        }
        return str_pad($number, 2, "0", STR_PAD_LEFT);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y');
    }

}
