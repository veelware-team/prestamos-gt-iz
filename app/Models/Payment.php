<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected  $guarded = [];

    protected $dates = [
        'created_at',
        'estimated_payment_date' ,
        'payment_date',
        'nullified_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y');
    }

    public function loand()
    {
        return $this->belongsTo(Loand::class);
    }

    public function collector(){
        return $this->hasOne(
            User::class,
            'id',
            'collected_by'
        );
    }

    public function nullifier(){
        return $this->hasOne(
            User::class,
            'id',
            'nullified_by'
        );
    }

    public function prevCollector(){
        return $this->hasOne(
            User::class,
            'id',
            'prev_collected_by'
        );
    }

    public static function nextCorrelative(){
        //PG-21-1-00001
        $year = Carbon::now()->format('y');
        $serialNumber = 1;
        $counter = 1;
        $lastPayment = Payment::query()
            ->where('correlative', 'like', '%PG-'.$year.'%')
            ->orderBy('correlative','DESC')
            ->first();

        if($lastPayment){
            $counterLastPayment = explode("-",$lastPayment->correlative)[3];
            $serialNumberLastPayment  = explode("-",$lastPayment->correlative)[2];
            if((int)$counterLastPayment+1 === 100000){
                $counter = 1;
                $serialNumber = (int)$serialNumberLastPayment+1;
            }else{
                $counter = (int)$counterLastPayment+1;
                $serialNumber = (int)$serialNumberLastPayment;
            }
        }
        $correlative = "PG-".$year."-"
            .$serialNumber."-".
            str_pad($counter, 5, "0", STR_PAD_LEFT);
        return $correlative;
    }

}
