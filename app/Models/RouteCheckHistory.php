<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RouteCheckHistory extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $dates = ['created_at', 'date'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y');
    }

    public function openingUser(){
        return $this->hasOne(
            User::class,
            'id',
            'open_by'
        );
    }

    public function closingUser(){
        return $this->hasOne(
            User::class,
            'id',
            'closed_by'
        );
    }

    public function route(){
        return $this->hasOne(
            Route::class,
            'id',
            'route_id'
        );
    }

    public function collector(){
        return $this->hasOne(
            User::class,
            'id',
            'cobrador_id'
        );
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

}
