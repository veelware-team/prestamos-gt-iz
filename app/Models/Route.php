<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    use HasFactory;
    protected  $guarded = [];

    public function departamento(){
        return $this->hasOne(Departamento::class,'id','departamento_id');
    }

    public function cobrador(){
        return $this->belongsTo(User::class,'cobrador_id','id');
    }

    public function municipio()
    {
        return $this->hasOne(Municipio::class,'id','municipio_id');
    }

    public function loands()
    {
        return $this->hasMany(Loand::class);
    }


    public function sectors()
    {
            return $this->belongsToMany(Sector::class);
    }
}
