<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function municipio()
    {
        return $this->belongsTo(Municipio::class);
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class);
    }

    public function routes()
    {
        return $this->belongsToMany(Route::class);
    }
}
