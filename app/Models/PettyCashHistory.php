<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PettyCashHistory extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $dates = [
        'date',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d-m-Y');
    }

    public function openingUser(){
        return $this->hasOne(
            User::class,
            'id',
            'open_by'
        );
    }

    public function closingUser(){
        return $this->hasOne(
            User::class,
            'id',
            'closed_by'
        );
    }

    public function dailyIncomes()
    {
        return $this->hasMany(DailyIncome::class);
    }

    public function dailySpends()
    {
        return $this->hasMany(DailySpend::class);
    }
    public static function nextCorrelative(){
        //HCC-21-05-14
        $date = Carbon::now()->format('y-m-d');
        $correlative = "HCC-".$date;
        return $correlative;
    }

}
