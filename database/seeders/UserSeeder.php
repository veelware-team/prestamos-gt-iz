<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarioSuperAdmin = User::create([
            'username' => 'whernandez',
            'firstName' => 'Walter',
            'lastName' => 'Hernández',
            'identificationNumber' => '23321231233123',
            'email' => 'admin@correo.com',
            'phoneOne' => '45459900',
            'password' => Hash::make('12345'),
            'remember_token' => Str::random(10),
        ]);
        $usuarioSuperAdmin->assignRole('superAdmin');

        $usuarioSuper2 = User::create([
            'username' => 'mproquimsa',
            'firstName' => 'Mario',
            'lastName' => 'González',
            'identificationNumber' => '23234455',
            'email' => 'help@proquimsa.com',
            'phoneOne' => '45459900',
            'password' => Hash::make('12345'),
            'remember_token' => Str::random(10),
            'active' => true,
        ]);
        $usuarioSuper2->assignRole('superAdmin');
        $url = '/images/user-masculino.png';
        $usuarioSuper2->image()->create(['url'=> $url]);

        /*$usuarioAdmin = User::create([
            'username' => 'osamayoa',
            'firstName' => 'Otoniel',
            'lastName' => 'Samayoa',
            'identificationNumber' => '2313987191801',
            'email' => '',
            'phoneOne' => '5966-3146',
            'password' => Hash::make('12345'),
            'remember_token' => Str::random(10),
            'active' => true,
        ]);
        $usuarioAdmin->assignRole('administrador');
        $url = '/images/user-masculino.png';
        $usuarioAdmin->image()->create(['url'=> $url]);*/
    }
}
