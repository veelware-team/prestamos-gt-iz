<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    // $role = Role::where('name','=','administrador')->first();
    public function run()
    {
        $rolSuperAdmin = Role::create(['name'=>'superAdmin']);
        $rolAdmin = Role::create(['name'=>'administrador']);
        $rolSupervisor = Role::create(['name'=>'supervisor']);
        $rolCobrador = Role::create(['name'=>'cobrador']);
    }
}
