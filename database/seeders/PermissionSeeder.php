<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleSuperAdmin = Role::where('name','=','superAdmin')->first();
        $roleAdmin = Role::where('name','=','administrador')->first();
        $roleSup = Role::where('name','=','supervisor')->first();
        $roleCob = Role::where('name','=','cobrador')->first();

        $soloAdmins = [$roleAdmin,$roleSuperAdmin];
        $adminsYsup = [$roleAdmin,$roleSuperAdmin,$roleSup];
        $cobradores = [$roleCob];
        $todos = [$roleAdmin,$roleSuperAdmin,$roleSup,$roleCob];

        DB::table('permissions')->delete();

        Permission::create(['name'=>'global.superAdmin','description'=>'Permiso superAdmin'])->syncRoles($roleSuperAdmin);
        Permission::create(['name'=>'global.admin','description'=>'Permiso global Administrador'])->syncRoles($soloAdmins);
        Permission::create(['name'=>'global.sup','description'=>'Permiso global Supervisor'])->syncRoles([$roleSup]);
        Permission::create(['name'=>'global.cob','description'=>'Permiso global Cobrador'])->syncRoles([$roleCob]);

        //kpis
        Permission::create(['name'=>'kpi.newCustomers','description'=>'kpi clientes nuevos'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'kpi.activeCustomers','description'=>'kpi clientes activos'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'kpi.totalCustomers','description'=>'kpi clientes totales'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'kpi.loanToApprove','description'=>'prestamos por aprobar'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'kpi.activeLoans','description'=>'prestamos activos'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'kpi.loansInArrears','description'=>'prestamos en mora'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'kpi.dailyRouteAmount','description'=>'Monto total a cobrar ruta diaria'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'kpi.dailyRouteAmountCharged','description'=>'Monto total cobrado ruta diaria'])->syncRoles($adminsYsup);

        // Configuraciones
        Permission::create(['name'=>'settings.view','description'=>'Configuraciones'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'roles.index','description'=>'Roles disponibles'])->syncRoles($adminsYsup);

        // Usuarios
        Permission::create(['name'=>'users.view','description'=>'Pantalla usuarios'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'users.collectors','description'=>'Cobradores disponibles'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'users.new','description'=>'Crear usuarios'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'users.update','description'=>'Editar usuarios'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'users.changePass','description'=>'Cambiar contraseña a su usuario'])->syncRoles($todos);

        //Rutas
        Permission::create(['name'=>'routes.view','description'=>'Pantalla rutas'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'routes.new','description'=>'Crear rutas'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'routes.update','description'=>'Editar rutas'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'routes.destroy','description'=>'Eliminar rutas'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'routes.sort','description'=>'Ordenar rutas'])->syncRoles($adminsYsup);


        //Departamentos
        Permission::create(['name'=>'departamentos.view','description'=>'Pantalla departamentos'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'departamentos.update','description'=>'Editar departamento'])->syncRoles($adminsYsup);

        //Municipios
        Permission::create(['name'=>'municipios.view','description'=>'Pantalla municipios'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'municipios.update','description'=>'Editar municipios'])->syncRoles($adminsYsup);

        //Sectores
        Permission::create(['name'=>'sectores.view','description'=>'Pantalla sectores'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'sectores.new','description'=>'Crear cliente'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'sectores.update','description'=>'Editar cliente'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'sectores.destroy','description'=>'Eliminar cliente'])->syncRoles($adminsYsup);

        //Clientes
        Permission::create(['name'=>'customers.view','description'=>'Listado cliente'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'customers.new','description'=>'Crear cliente'])->syncRoles($todos);
        Permission::create(['name'=>'customers.show','description'=>'Ver cliente'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'customers.update','description'=>'Editar cliente'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'customers.destroy','description'=>'Eliminar cliente'])->syncRoles($soloAdmins);

        //Prestamos
        Permission::create(['name'=>'loands.view','description'=>'Listado Prestamo'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'loands.new','description'=>'Crear Prestamo'])->syncRoles($todos);
        Permission::create(['name'=>'loands.show','description'=>'Ver Prestamo'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'loands.update','description'=>'Editar Prestamo'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'loands.approve','description'=>'Aprobar Prestamo'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'loands.reject','description'=>'Rechazar Prestamo'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'loands.nullify','description'=>'Anular Prestamo'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'loands.activate','description'=>'Activar Prestamo'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'loands.destroy','description'=>'Eliminar Prestamo'])->syncRoles($adminsYsup);

        //Refinanciamientos
        Permission::create(['name'=>'refinancedLoands.view','description'=>'Listado de Refinanciamientos'])->syncRoles($adminsYsup);

        //Cuotas y Pagos
        Permission::create(['name'=>'payments.view','description'=>'Listado de pagos'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'payments.cancelArrears','description'=>'Anular mora Pago'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'payments.cancelInterest','description'=>'Anular Interes Pago'])->syncRoles($soloAdmins);
        Permission::create(['name'=>'payments.pay','description'=>'Pagar cuotas'])->syncRoles($todos);
        Permission::create(['name'=>'payments.reversion','description'=>'Revertir cuotas'])->syncRoles($soloAdmins);

        //Caja chica
        Permission::create(['name'=>'pettyCashHistories.menu','description'=>'Menu caja chica'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'pettyCashHistories.view','description'=>'Listado historial caja chica'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'pettyCashHistories.dailyView','description'=>'Caja chica diaria'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'pettyCashHistories.new','description'=>'Caja chica diaria crear'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'pettyCashHistories.close','description'=>'Cerrar Caja chica diaria'])->syncRoles($adminsYsup);

        //Ingresos
        Permission::create(['name'=>'dailyIncomes.view','description'=>'Listado de ingresos'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'dailyIncomes.totalDaily','description'=>'Ingresos diarios'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'dailyIncomes.new','description'=>'Crear ingreso diario'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'dailyIncomes.cancel','description'=>'Cancelar ingreso diario'])->syncRoles($adminsYsup);

        //Retiros
        Permission::create(['name'=>'dailySpends.view','description'=>'Listado de ingresos'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'dailySpends.totalDaily','description'=>'Ingresos diarios'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'dailySpends.new','description'=>'Crear ingreso diario'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'dailySpends.cancel','description'=>'Cancelar ingreso diario'])->syncRoles($adminsYsup);

        //Cuadre de rutas
        Permission::create(['name'=>'routeCheckHistories.menu','description'=>'Menu cuadre rutas'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'routeCheckHistories.view','description'=>'Historial de cuadre de rutas'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'routeCheckHistories.routesAvailables','description'=>'Rutas disponibles para cuadre'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'routeCheckHistories.checkSingle','description'=>'Cuadrar ruta'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'routeCheckHistories.new','description'=>'Guardar Cuadrar ruta'])->syncRoles($adminsYsup);

        //Reportes
        Permission::create(['name'=>'reports.menu','description'=>'Menu reporte'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'reports.approvedLoans','description'=>'Reporte prestamos aprobados'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'reports.arrearsFees','description'=>'Reporte Cuotas atrasadas'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'reports.paidLoans','description'=>'Reporte Prestamos pagados'])->syncRoles($adminsYsup);
        Permission::create(['name'=>'reports.typeLoans','description'=>'Reporte Prestamos por tipo'])->syncRoles($adminsYsup);

    }
}
