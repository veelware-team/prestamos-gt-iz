<?php

namespace Database\Seeders;

use App\Models\Municipio;
use Illuminate\Database\Seeder;

class MunicipioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //         Alta Verapaz
        Municipio::create(['name'=>'Chahal', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Chisec', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Cobán', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Fray Bartolomé de las Casas', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'La Tinta', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Lanquín', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Panzós', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Raxruhá', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'San Cristóbal Verapaz', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'San Juan Chamelco', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'San Pedro Carchá', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Santa Cruz Verapaz', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Santa María Cahabón', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Senahú', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Tamahú', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Tactic', 'departamento_id'=> 1]);
        Municipio::create(['name'=>'Tucurú', 'departamento_id'=> 1]);

//       Baja Verapaz
        Municipio::create(['name'=>'Cubulco', 'departamento_id'=> 2]);
        Municipio::create(['name'=>'Granados', 'departamento_id'=> 2]);
        Municipio::create(['name'=>'Purulhá', 'departamento_id'=> 2]);
        Municipio::create(['name'=>'Rabinal', 'departamento_id'=> 2]);
        Municipio::create(['name'=>'Salamá', 'departamento_id'=> 2]);
        Municipio::create(['name'=>'San Jerónimo', 'departamento_id'=> 2]);
        Municipio::create(['name'=>'San Miguel Chicaj', 'departamento_id'=> 2]);
        Municipio::create(['name'=>'Santa Cruz el Chol', 'departamento_id'=> 2]);

//       Chimaltenango
        Municipio::create(['name'=>'Acatenango', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Chimaltenango', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'El Tejar', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Parramos', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Patzicía', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Patzún', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Pochuta', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'San Andrés Itzapa', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'San José Poaquíl', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'San Juan Comalapa', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'San Martín Jilotepeque', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Santa Apolonia', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Santa Cruz Balanyá', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Tecpán', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Yepocapa', 'departamento_id'=> 3]);
        Municipio::create(['name'=>'Zaragoza', 'departamento_id'=> 3]);

//       Chiquimula
        Municipio::create(['name'=>'Camotán', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'Chiquimula', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'Concepción Las Minas', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'Esquipulas', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'Ipala', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'Jocotán', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'Olopa', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'Quezaltepeque', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'San Jacinto', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'San José la Arada', 'departamento_id'=> 4]);
        Municipio::create(['name'=>'San Juan Ermita', 'departamento_id'=> 4]);

//       El Progreso
        Municipio::create(['name'=>'El Jícaro', 'departamento_id'=> 5]);
        Municipio::create(['name'=>'Guastatoya', 'departamento_id'=> 5]);
        Municipio::create(['name'=>'Morazán', 'departamento_id'=> 5]);
        Municipio::create(['name'=>'San Agustín Acasaguastlán', 'departamento_id'=> 5]);
        Municipio::create(['name'=>'San Antonio La Paz', 'departamento_id'=> 5]);
        Municipio::create(['name'=>'San Cristóbal Acasaguastlán', 'departamento_id'=> 5]);
        Municipio::create(['name'=>'Sanarate', 'departamento_id'=> 5]);
        Municipio::create(['name'=>'Sansare', 'departamento_id'=> 5]);

//       Escuintla
        Municipio::create(['name'=>'Escuintla', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'Guanagazapa', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'Iztapa', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'La Democracia', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'La Gomera', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'Masagua', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'Nueva Concepción', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'Palín', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'San José', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'San Vicente Pacaya', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'Santa Lucía Cotzumalguapa', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'Siquinalá', 'departamento_id'=> 6]);
        Municipio::create(['name'=>'Tiquisate', 'departamento_id'=> 6]);

//       Guatemala
        Municipio::create(['name'=>'Amatitlán', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Chinautla', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Chuarrancho', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Guatemala', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Fraijanes', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Mixco', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Palencia', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'San José del Golfo', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'San José Pinula', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'San Juan Sacatepéquez', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'San Miguel Petapa', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'San Pedro Ayampuc', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'San Pedro Sacatepéquez', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'San Raymundo', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Santa Catarina Pinula', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Villa Canales', 'departamento_id'=> 7]);
        Municipio::create(['name'=>'Villa Nueva', 'departamento_id'=> 7]);

//       Huehuetenango
        Municipio::create(['name'=>'Aguacatán', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Chiantla', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Colotenango', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Concepción Huista', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Cuilco', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Huehuetenango', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Jacaltenango', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'La Democracia', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'La Libertad', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Malacatancito', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Nentón', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Antonio Huista', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Gaspar Ixchil', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Ildefonso Ixtahuacán', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Juan Atitán', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Juan Ixcoy', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Mateo Ixtatán', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Miguel Acatán', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Pedro Nécta', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Pedro Soloma', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Rafael La Independencia', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Rafael Pétzal', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Sebastián Coatán', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'San Sebastián Huehuetenango', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Santa Ana Huista', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Santa Bárbara', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Santa Cruz Barillas', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Santa Eulalia', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Santiago Chimaltenango', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Tectitán', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Todos Santos Cuchumatán', 'departamento_id'=> 8]);
        Municipio::create(['name'=>'Unión Cantinil', 'departamento_id'=> 8]);

//       Izabal
        Municipio::create(['name'=>'El Estor', 'departamento_id'=> 9]);
        Municipio::create(['name'=>'Livingston', 'departamento_id'=> 9]);
        Municipio::create(['name'=>'Los Amates', 'departamento_id'=> 9]);
        Municipio::create(['name'=>'Morales', 'departamento_id'=> 9]);
        Municipio::create(['name'=>'Puerto Barrios', 'departamento_id'=> 9]);

//       Jalapa
        Municipio::create(['name'=>'Jalapa', 'departamento_id'=> 10]);
        Municipio::create(['name'=>'Mataquescuintla', 'departamento_id'=> 10]);
        Municipio::create(['name'=>'Monjas', 'departamento_id'=> 10]);
        Municipio::create(['name'=>'San Carlos Alzatate', 'departamento_id'=> 10]);
        Municipio::create(['name'=>'San Luis Jilotepeque', 'departamento_id'=> 10]);
        Municipio::create(['name'=>'San Manuel Chaparrón', 'departamento_id'=> 10]);
        Municipio::create(['name'=>'San Pedro Pinula', 'departamento_id'=> 10]);

//       Jutiapa
        Municipio::create(['name'=>'Agua Blanca', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Asunción Mita', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Atescatempa', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Comapa', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Conguaco', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'El Adelanto', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'El Progreso', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Jalpatagua', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Jerez', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Jutiapa', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Moyuta', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Pasaco', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Quesada', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'San José Acatempa', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Santa Catarina Mita', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Yupiltepeque', 'departamento_id'=> 11]);
        Municipio::create(['name'=>'Zapotitlán', 'departamento_id'=> 11]);

//       Petén
        Municipio::create(['name'=>'Dolores', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'El Chal', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'Ciudad Flores', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'La Libertad', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'Las Cruces', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'Melchor de Mencos', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'Poptún', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'San Andrés', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'San Benito', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'San Francisco', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'San José', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'San Luis', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'Santa Ana', 'departamento_id'=> 12]);
        Municipio::create(['name'=>'Sayaxché', 'departamento_id'=> 12]);

//       Quetzaltenango
        Municipio::create(['name'=>'Almolonga', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Cabricán', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Cajolá', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Cantel', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Coatepeque', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Colomba Costa Cuca', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Concepción Chiquirichapa', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'El Palmar', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Flores Costa Cuca', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Génova', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Huitán', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'La Esperanza', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Olintepeque', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Palestina de Los Altos', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Quetzaltenango', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Salcajá', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'San Carlos Sija', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'San Francisco La Unión', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'San Juan Ostuncalco', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'San Martín Sacatepéquez', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'San Mateo', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'San Miguel Sigüilá', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Sibilia', 'departamento_id'=> 13]);
        Municipio::create(['name'=>'Zunil', 'departamento_id'=> 13]);

//       Quiché
        Municipio::create(['name'=>'Canillá', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Chajul', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Chicamán', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Chiché', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Chichicastenango', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Chinique', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Cunén', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Ixcán Playa Grande', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Joyabaj', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Nebaj', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Pachalum', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Patzité', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Sacapulas', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'San Andrés Sajcabajá', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'San Antonio Ilotenango', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'San Bartolomé Jocotenango', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'San Juan Cotzal', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'San Pedro Jocopilas', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Santa Cruz del Quiché', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Uspantán', 'departamento_id'=> 14]);
        Municipio::create(['name'=>'Zacualpa', 'departamento_id'=> 14]);

//       Retalhuleu
        Municipio::create(['name'=>'Champerico', 'departamento_id'=> 15]);
        Municipio::create(['name'=>'El Asintal', 'departamento_id'=> 15]);
        Municipio::create(['name'=>'Nuevo San Carlos', 'departamento_id'=> 15]);
        Municipio::create(['name'=>'Retalhuleu', 'departamento_id'=> 15]);
        Municipio::create(['name'=>'San Andrés Villa Seca', 'departamento_id'=> 15]);
        Municipio::create(['name'=>'San Felipe Reu', 'departamento_id'=> 15]);
        Municipio::create(['name'=>'San Martín Zapotitlán', 'departamento_id'=> 15]);
        Municipio::create(['name'=>'San Sebastián', 'departamento_id'=> 15]);
        Municipio::create(['name'=>'Santa Cruz Muluá', 'departamento_id'=> 15]);

//       Sacatepéquez
        Municipio::create(['name'=>'Alotenango', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Ciudad Vieja', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Jocotenango', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Antigua Guatemala', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Magdalena Milpas Altas', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Pastores', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'San Antonio Aguas Calientes', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'San Bartolomé Milpas Altas', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'San Lucas Sacatepéquez', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'San Miguel Dueñas', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Santa Catarina Barahona', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Santa Lucía Milpas Altas', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Santa María de Jesús', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Santiago Sacatepéquez', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Santo Domingo Xenacoj', 'departamento_id'=> 16]);
        Municipio::create(['name'=>'Sumpango', 'departamento_id'=> 16]);

//       San Marcos
        Municipio::create(['name'=>'Ayutla', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Catarina', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Comitancillo', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Concepción Tutuapa', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'El Quetzal', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'El Tumbador', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Esquipulas Palo Gordo', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Ixchiguán', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'La Blanca', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'La Reforma', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Malacatán', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Nuevo Progreso', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Ocós', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Pajapita', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Río Blanco', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San Antonio Sacatepéquez', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San Cristóbal Cucho', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San José El Rodeo', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San José Ojetenam', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San Lorenzo', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San Marcos', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San Miguel Ixtahuacán', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San Pablo', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San Pedro Sacatepéquez', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'San Rafael Pie de la Cuesta', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Sibinal', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Sipacapa', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Tacaná', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Tajumulco', 'departamento_id'=> 17]);
        Municipio::create(['name'=>'Tejutla', 'departamento_id'=> 17]);

//     Santa Rosa
        Municipio::create(['name'=>'Barberena', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Casillas', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Chiquimulilla', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Cuilapa', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Guazacapán', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Nueva Santa Rosa', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Oratorio', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Pueblo Nuevo Viñas', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'San Juan Tecuaco', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'San Rafael las Flores', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Santa Cruz Naranjo', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Santa María Ixhuatán', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Santa Rosa de Lima', 'departamento_id'=> 18]);
        Municipio::create(['name'=>'Taxisco', 'departamento_id'=> 18]);

//     Sololá
        Municipio::create(['name'=>'Concepción', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Nahualá', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Panajachel', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'San Andrés Semetabaj', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'San Antonio Palopó', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'San José Chacayá', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'San Juan La Laguna', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'San Lucas Tolimán', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'San Marcos La Laguna', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'San Pablo La Laguna', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'San Pedro La Laguna', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Santa Catarina Ixtahuacán', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Santa Catarina Palopó', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Santa Clara La Laguna', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Santa Cruz La Laguna', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Santa Lucía Utatlán', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Santa María Visitación', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Santiago Atitlán', 'departamento_id'=> 19]);
        Municipio::create(['name'=>'Sololá', 'departamento_id'=> 19]);

//     Suchitepéquez
        Municipio::create(['name'=>'Chicacao', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Cuyotenango', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Mazatenango', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Patulul', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Pueblo Nuevo', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Río Bravo', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Samayac', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San Antonio Suchitepéquez', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San Bernardino', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San Francisco Zapotitlán', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San Gabriel', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San José El Idolo', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San José La Maquina', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San Juan Bautista', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San Lorenzo', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San Miguel Panán', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'San Pablo Jocopilas', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Santa Bárbara', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Santo Domingo Suchitepéquez', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Santo Tomás La Unión', 'departamento_id'=> 20]);
        Municipio::create(['name'=>'Zunilito', 'departamento_id'=> 20]);

//     Totonicapán
        Municipio::create(['name'=>'Momostenango', 'departamento_id'=> 21]);
        Municipio::create(['name'=>'San Andrés Xecul', 'departamento_id'=> 21]);
        Municipio::create(['name'=>'San Bartolo', 'departamento_id'=> 21]);
        Municipio::create(['name'=>'San Cristóbal Totonicapán', 'departamento_id'=> 21]);
        Municipio::create(['name'=>'San Francisco El Alto', 'departamento_id'=> 21]);
        Municipio::create(['name'=>'Santa Lucía La Reforma', 'departamento_id'=> 21]);
        Municipio::create(['name'=>'Santa María Chiquimula', 'departamento_id'=> 21]);
        Municipio::create(['name'=>'Totonicapán', 'departamento_id'=> 21]);

//     Zacapa
        Municipio::create(['name'=>'Cabañas', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'Estanzuela', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'Gualán', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'Huité', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'La Unión', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'Río Hondo', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'San Diego', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'San Jorge', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'Teculután', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'Usumatlán', 'departamento_id'=> 22]);
        Municipio::create(['name'=>'Zacapa', 'departamento_id'=> 22]);
    }
}
