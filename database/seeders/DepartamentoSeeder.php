<?php

namespace Database\Seeders;

use App\Models\Departamento;
use Illuminate\Database\Seeder;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Departamento::create(['name'=> 'Alta Verapaz']);
        Departamento::create(['name'=> 'Baja Verapaz']);
        Departamento::create(['name'=> 'Chimaltenango']);
        Departamento::create(['name'=> 'Chiquimula']);
        Departamento::create(['name'=> 'El Progreso']);
        Departamento::create(['name'=> 'Escuintla']);
        Departamento::create(['name'=> 'Guatemala']);
        Departamento::create(['name'=> 'Huehuetenango']);
        Departamento::create(['name'=> 'Izabal']);
        Departamento::create(['name'=> 'Jalapa']);
        Departamento::create(['name'=> 'Jutiapa']);
        Departamento::create(['name'=> 'Petén']);
        Departamento::create(['name'=> 'Quetzaltenango']);
        Departamento::create(['name'=> 'Quiché']);
        Departamento::create(['name'=> 'Retalhuleu']);
        Departamento::create(['name'=> 'Sacatepéquez']);
        Departamento::create(['name'=> 'San Marcos']);
        Departamento::create(['name'=> 'Santa Rosa']);
        Departamento::create(['name'=> 'Solola']);
        Departamento::create(['name'=> 'Suchitepéquez']);
        Departamento::create(['name'=> 'Totonicapán']);
        Departamento::create(['name'=> 'Zacapa']);
    }
}
