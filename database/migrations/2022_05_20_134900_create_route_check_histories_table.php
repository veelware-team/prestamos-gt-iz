<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRouteCheckHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_check_histories', function (Blueprint $table) {
            $table->id();
            $table->string("correlative");
            $table->date("date");
            $table->foreignId('route_id')->constrained();
            $table->foreignId('cobrador_id')->constrained('users', 'id');
            $table->string('state');
            $table->double('amount_to_collect',11,2)->nullable();
            $table->integer('fees_to_collect')->nullable();
            $table->double('amount_collected',11,2)->nullable();
            $table->integer('fees_collected')->nullable();
            $table->double('net_amount_collected',11,2)->nullable();
            $table->double('late_fee_amount_collected',11,2)->nullable();
            $table->double('amount_collected_extraordinary',11,2)->nullable();
            $table->integer('fees_collected_extraordinary')->nullable();
            $table->foreignId('open_by')->nullable()->constrained('users', 'id');
            $table->foreignId('closed_by')->nullable()->constrained('users', 'id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_check_histories');
    }
}
