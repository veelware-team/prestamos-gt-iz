<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailySpendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // $egreso = DailySpend::create([ 'petty_cash_history_id'=> 1, 'correlative'=>'EG-21-05-14-001', 'date'=> carbon::now(), 'amount'=> 2000, 'note'=>'NOTA EGRESO', 'created_by'=> 1, 'state'=> "REGISTRADO"]);
    public function up()
    {
        Schema::create('daily_spends', function (Blueprint $table) {
            $table->id();
            $table->foreignId('petty_cash_history_id')->constrained('petty_cash_histories', 'id');
            $table->string("correlative");
            $table->date("date");
            $table->double('amount',11,2);
            $table->text("note");
            $table->foreignId('created_by')->nullable()->constrained('users', 'id');
            $table->foreignId('nullified_by')->nullable()->constrained('users', 'id');
            $table->date('nullified_at')->nullable();
            $table->text('annulment_note')->nullable();
            $table->string('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_spends');
    }
}
