<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('loand_id')->constrained();
            $table->string("correlative")->nullable();
            $table->date("estimated_payment_date");
            $table->date("payment_date")->nullable();
            $table->double('capital_payment',11,2);
            $table->double('interest_payment',11,2);
            $table->double('late_fee',8,2)->nullable();
            $table->double('total_payment',11,2);
            $table->string("state");
            $table->foreignId('collected_by')->nullable()->constrained('users', 'id');
            $table->text("collection_note")->nullable();

            $table->foreignId('nullified_by')->nullable()->constrained('users', 'id');
            $table->dateTime('nullified_at')->nullable();
            $table->text('annulment_note')->nullable();

            $table->boolean('was_annulled')->default(false);
            $table->foreignId('prev_collected_by')->nullable()->constrained('users', 'id');
            $table->text("prev_collection_note")->nullable();

            $table->boolean('no_late_fee')->default(false);
            $table->boolean('no_interest_payment')->default(false);

            $table->boolean('paid_out')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
