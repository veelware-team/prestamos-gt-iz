<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('correlative');
            $table->string('identificationNumber');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('phoneOne');
            $table->string('phoneTwo')->nullable();
            $table->string('gender');
            $table->foreignId('departamento_id')->constrained();
            $table->foreignId('municipio_id')->constrained();
            $table->foreignId('sector_id')->constrained();
            $table->double('rating')->nullable();
            $table->text('address');
            $table->text('referenceAddress')->nullable();
            $table->foreignId('created_by')->constrained('users', 'id');
            $table->timestamps();
            $table->foreignId('deactivated_by')->nullable()->constrained('users', 'id');
            $table->dateTime('deactivated_at')->nullable();
            $table->text('deactivation_note')->nullable();
            $table->boolean('active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
