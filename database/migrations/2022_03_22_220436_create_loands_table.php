<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // $loand = Loand:create([ 'correlative'=>'P001', 'customer_id'=> 2, 'type'=> "DIARIO", 'state'=> "PENDIENTE",'dues'=> 20, 'interest_percentage'=> 15, 'borrowed_amount'=> 1500, 'interest_amount'=> 500,'total_amount'=> 2000,'fixed_fee'=> 100,'created_by'=> 1 ]);
    public function up()
    {
        Schema::create('loands', function (Blueprint $table) {
            $table->id();
            $table->string('correlative');
            $table->foreignId('customer_id')->constrained();
            $table->string('business_name')->nullable();
            $table->string('type');
            $table->string('state');
            $table->integer('dues');
            $table->integer('interest_percentage');
            $table->text('loan_purpose')->nullable();
            $table->dateTime('first_payment_date')->nullable();
            $table->double('late_fee',8,2)->nullable();
            $table->double('borrowed_amount',11,2);
            $table->double('interest_amount',11,2);
            $table->double('total_amount',11,2);
            $table->double('fixed_fee',11,2);
            $table->foreignId('created_by')->constrained('users', 'id');
            $table->foreignId('confirmed_by')->nullable()->constrained('users', 'id');
            $table->dateTime('confirmed_at')->nullable();
            $table->foreignId('nullified_by')->nullable()->constrained('users', 'id');
            $table->dateTime('nullified_at')->nullable();
            $table->text('annulment_note')->nullable();
            $table->foreignId('rejected_by')->nullable()->constrained('users', 'id');
            $table->dateTime('rejected_at')->nullable();
            $table->text('rejection_note')->nullable();
            //datos para ruta etc
            $table->boolean('use_customer_information')->default(false);
            $table->string('phoneOne');
            $table->string('phoneTwo')->nullable();
            $table->foreignId('departamento_id')->constrained();
            $table->foreignId('municipio_id')->constrained();
            $table->foreignId('sector_id')->constrained();
            $table->foreignId('route_id')->nullable()->constrained();
            $table->text('address');
            $table->text('referenceAddress')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loands');
    }
}
