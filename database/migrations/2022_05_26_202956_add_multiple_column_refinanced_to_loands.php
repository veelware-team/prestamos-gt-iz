<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColumnRefinancedToLoands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loands', function (Blueprint $table) {
            $table->double('pending_amount',11,2)->nullable();
            $table->double('additional_amount',11,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loands', function (Blueprint $table) {
            $table->dropColumn('pending_amount');
            $table->dropColumn('additional_amount');
        });
    }
}
