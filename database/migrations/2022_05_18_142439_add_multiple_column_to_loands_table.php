<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColumnToLoandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loands', function (Blueprint $table) {
            $table->foreignId('loand_parent_id')
                ->nullable()
                ->constrained('loands','id');
            $table->integer('order')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loands', function (Blueprint $table) {
            $table->dropColumn('order');
            $table->dropForeign(['loand_parent_id']);
            $table->dropColumn('loand_parent_id');
        });
    }
}
