<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePettyCashHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // $pettyCash = PettyCashHistory::create([ 'correlative'=>'HCC-21-05-14', 'date'=> carbon::now(), 'initial_balance'=> 2000, 'total_daily_income'=> 100, 'total_fees_collected'=> 250.25, 'total_daily_withdrawals'=> 250.25, 'total_loans_granted'=> 250.25, 'ending_balance'=> 250.25, 'open_by'=> 1, 'state'=> "ABIERTA"]);

    public function up()
    {
        Schema::create('petty_cash_histories', function (Blueprint $table) {
            $table->id();
            $table->string("correlative");
            $table->date("date");
            $table->double('initial_balance',11,2);
            $table->double('total_daily_income',11,2)->nullable();
            $table->double('total_fees_collected',11,2)->nullable();
            $table->double('total_daily_withdrawals',11,2)->nullable();
            $table->double('total_loans_granted',11,2)->nullable();
            $table->double('ending_balance',11,2)->nullable();
            $table->foreignId('open_by')->nullable()->constrained('users', 'id');
            $table->foreignId('closed_by')->nullable()->constrained('users', 'id');
            $table->string('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petty_cash_histories');
    }
}
