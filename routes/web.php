<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\SettingsController;
use \App\Http\Controllers\DepartamentoController;
use \App\Http\Controllers\MunicipioController;
use \App\Http\Controllers\SectorController;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\RouteController;
use \App\Http\Controllers\CustomerController;
use \App\Http\Controllers\LoandController;
use \App\Http\Controllers\PaymentController;
use App\Http\Controllers\PettyCashHistoryController;
use App\Http\Controllers\DailyIncomeController;
use App\Http\Controllers\DailySpendController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RouteCheckHistoryController;
use App\Http\Controllers\ReportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\HomeController::class,'index'])->name('index');

Route::group(['prefix'=>'admin','middleware'=>'auth'],function (){

    Route::get('/dashboard', [DashboardController::class,'view'])->name('dashboard');

    Route::get('/kpi/cliente-nuevos', [DashboardController::class,'newCustomers'])
        ->name('kpi.newCustomers');

    Route::get('/kpi/cliente-activos', [DashboardController::class,'activeCustomers'])
        ->name('kpi.activeCustomers');

    Route::get('/kpi/cliente-totales', [DashboardController::class,'totalCustomers'])
        ->name('kpi.totalCustomers');

    Route::get('/kpi/prestamos-por-aprobar', [DashboardController::class,'loanToApprove'])
        ->name('kpi.loanToApprove');

    Route::get('/kpi/prestamos-activos', [DashboardController::class,'activeLoans'])
        ->name('kpi.activeLoans');

    Route::get('/kpi/prestamos-en-mora', [DashboardController::class,'loansInArrears'])
        ->name('kpi.loansInArrears');

    Route::get('/kpi/monto-ruta-diaria', [DashboardController::class,'dailyRouteAmount'])
        ->name('kpi.dailyRouteAmount');

    Route::get('/kpi/monto-ruta-diaria-cobrado', [DashboardController::class,'dailyRouteAmountCharged'])
        ->name('kpi.dailyRouteAmountCharged');

    /*Route::get('/test', [DashboardController::class,'testQuerys'])
        ->name('kpi.testQuerys');*/

    Route::get('configuraciones',[SettingsController::class,'view'])
        ->name('settings.view');

    //Departamentos
    Route::get('configuraciones/listado-departamentos',[DepartamentoController::class,'view'])
        ->name('departamentos.view');

    Route::resource('configuraciones/departamentos', DepartamentoController::class)
        ->parameters(['departamento'=>'departamento'])->only(['index','update'])
        ->names('departamentos');

    //Municipios
    Route::get('configuraciones/listado-municipios',[MunicipioController::class,'view'])
        ->name('municipios.view');

    Route::resource('configuraciones/municipios', MunicipioController::class)
        ->parameters(['municipio'=>'municipio'])->only(['index','update'])
        ->names('municipios');


    //Sectores
    Route::get('configuraciones/listado-sectores',[SectorController::class,'view'])
        ->name('sectores.view');

    Route::resource('configuraciones/sector', SectorController::class)
        ->parameters(['sector'=>'sector'])->only(['index','store','update','destroy'])
        ->names('sectores');

    // Roles
    //disponibles
    Route::get('configuraciones/listado-roles',[UserController::class,'availableRoles'])
        ->name('roles.index');

    //Usuarios
    // -- Cobradores
    Route::get('configuraciones/usuarios/collectors',[UserController::class,'collectorsAvailable'])
        ->name('users.collectors');

    Route::get('configuraciones/listado-usuarios',[UserController::class,'view'])
        ->name('users.view');
    // -- generar username
    Route::post('configuraciones/usuarios/username',[UserController::class,'generateUsername'])
        ->name('users.generate');

    // -- generar username
    Route::post('configuraciones/usuarios/cps',[UserController::class,'changePass'])
        ->name('users.changePass');

    Route::post('configuraciones/usuarios/rps',[UserController::class,'resetPass'])
        ->name('users.resetPass');

    Route::resource('configuraciones/usuarios', UserController::class)
        ->parameters(['usuarios'=>'user'])->except(['show','destroy'])
        ->names('users');

    //Rutas

    Route::get('/listado-rutas',[RouteController::class,'view'])
        ->name('routes.view');

    Route::get('/rutas/correlativo', [RouteController::class,'generateCorrelative'])
        ->name('routes.correlative');

    Route::resource('/rutas', RouteController::class)
        ->parameters(['rutas'=>'route'])->except(['show'])
        ->names('routes');

    Route::get('/rutas/{route}/ordenar',[RouteController::class,'sort'])
        ->name('routes.sort');

    Route::post('/rutas/{route}/ordenar',[RouteController::class,'storeSort'])
        ->name('routes.storeSort');

    Route::get('rutas/{route}/next-day',[RouteController::class,'collectTomorrow'])
        ->name('routes.collectTomorrow');

    //Clientes

    Route::get('/listado-clientes', [CustomerController::class,'view'])
        ->name('customers.view');

    /*Route::get('/clientes/correlativo', [CustomerController::class,'generateCorrelative'])
        ->name('customers.correlative');*/

    Route::put('/clientes/estado/{customer}',[CustomerController::class,'state'])
        ->name('customers.state');

    Route::resource('/clientes', CustomerController::class)
        ->parameters(['clientes' =>'customer'])
        ->names('customers');

    //Prestamos y Refinanciamientos


    Route::get('/listado-refinanciamientos', [LoandController::class,'viewRefinanced'])
        ->name('refinancedLoands.view');

    Route::get('/refinanciamientos/create', [LoandController::class,'refinancedCreate'])
        ->name('refinancedLoands.create');

    Route::get('/refinanciamientos/{loand}/edit', [LoandController::class,'refinancedEdit'])
        ->name('refinancedLoands.edit');

    Route::get('/refinanciamientos', [LoandController::class,'refinancedIndex'])
        ->name('refinancedLoands.index');

    Route::post('prestamos/buscar', [LoandController::class,'search'])
        ->name('loands.search');

    Route::get('/listado-prestamos', [LoandController::class,'view'])
        ->name('loands.view');

    Route::put('/prestamos/aprobar/{loand}', [LoandController::class,'approve'])
        ->name('loands.approve');

    Route::put('/prestamos/rechazar/{loand}', [LoandController::class,'reject'])
        ->name('loands.reject');

    Route::put('/prestamos/anular/{loand}', [LoandController::class,'nullify'])
        ->name('loands.nullify');

    Route::post('/prestamos/activar/{loand}', [LoandController::class,'activate'])
        ->name('loands.activate');

    Route::get('/listado-prestamos/total-diario-otorgado', [LoandController::class,'totalDailyGranted'])
        ->name('loands.totalDailyGranted');



    Route::resource('/prestamos', LoandController::class)
        ->parameters(['prestamos' =>'loand'])
        ->names('loands');

    //Pagos
    Route::get('/listado-pagos', [PaymentController::class,'view'])
        ->name('payments.view');

    Route::get('/listado-pagos/total-diario-recaudado', [PaymentController::class,'totalDailyCollected'])
        ->name('payments.totalDailyCollected');

    Route::resource('/pagos', PaymentController::class)
        ->parameters(['pagos'=>'payment'])->only(['index'])
        ->names('payments');

    Route::post('/prestamos/{loand}/pago/{payment}/pagar', [PaymentController::class,'individualPay'])
        ->name('payments.individualPay');
    Route::post('/prestamos/{loand}/pago/{payment}/revertir', [PaymentController::class,'reversion'])
        ->name('payments.reversion');

    Route::get('/prestamos/{loand}/cuotas',[PaymentController::class,'paymentOfLoand'])
        ->name('payments.paymentOfLoand');

    //Caja chica
    Route::get('caja-chica/menu',[PettyCashHistoryController::class,'menu'])
        ->name('pettyCashHistories.menu');

    Route::get('caja-chica/listado',[PettyCashHistoryController::class,'view'])
        ->name('pettyCashHistories.view');

    Route::get('caja-chica/diaria',[PettyCashHistoryController::class,'dailyView'])
        ->name('pettyCashHistories.dailyView');

    Route::get('caja-chica/check',[PettyCashHistoryController::class,'check'])
        ->name('pettyCashHistories.check');

    Route::get('caja-chica/check-pending',[PettyCashHistoryController::class,'checkPending'])
        ->name('pettyCashHistories.checkPending');

    Route::post('caja-chica/{pettyCashHistory}/cerrar', [PettyCashHistoryController::class,'close'])
        ->name('pettyCashHistories.close');

    Route::resource('caja-chica', PettyCashHistoryController::class)
        ->parameters(['caja-chica'=>'pettyCashHistory'])->only(['store','index'])
        ->names('pettyCashHistories');

    //Ingresos de caja

    Route::get('listado-ingresos',[DailyIncomeController::class,'view'])
        ->name('dailyIncomes.view');

    Route::get('ingresos/diarios',[DailyIncomeController::class,'getTotalDaily'])
        ->name('dailyIncomes.totalDaily');

    Route::resource('/ingresos', DailyIncomeController::class)
        ->parameters(['ingresos'=>'dailyIncome'])->only(['store','index'])
        ->names('dailyIncomes');

    Route::post('/ingresos/{dailyIncome}/anular', [DailyIncomeController::class,'cancel'])
        ->name('dailyIncomes.cancel');

    //retiros de caja

    Route::get('listado-retiros',[DailySpendController::class,'view'])
        ->name('dailySpends.view');

    Route::get('retiros/diarios',[DailySpendController::class,'getTotalDaily'])
        ->name('dailySpends.totalDaily');

    Route::resource('/retiros', DailySpendController::class)
        ->parameters(['retiros'=>'dailySpend'])->only(['store','index'])
        ->names('dailySpends');

    Route::post('/retiros/{dailySpend}/anular', [DailySpendController::class,'cancel'])
        ->name('dailySpends.cancel');

    //Cuadre rutas
    Route::get('cuadre-rutas/menu',[RouteCheckHistoryController::class,'menu'])
        ->name('routeCheckHistories.menu');

    Route::get('cuadre-rutas/rutas',[RouteCheckHistoryController::class,'routesAvailables'])
        ->name('routeCheckHistories.routesAvailables');

    Route::get('cuadre-rutas/historial',[RouteCheckHistoryController::class,'view'])
        ->name('routeCheckHistories.view');

    Route::get('cuadre-rutas/{route}/cuadre',[RouteCheckHistoryController::class,'checkSingle'])
        ->name('routeCheckHistories.checkSingle');

    Route::get('cuadre-rutas/{route}/cuadre-valores',[RouteCheckHistoryController::class,'checkSingleValues'])
        ->name('routeCheckHistories.checkSingleValues');

    Route::get('cuadre-rutas/{route}/check',[RouteCheckHistoryController::class,'checkIfHistoryExist'])
        ->name('routeCheckHistories.checkIfHistoryExist');

    Route::get('cuadre-rutas/check-daily-all',[RouteCheckHistoryController::class,'checkDailyHistories'])
        ->name('routeCheckHistories.checkDailyHistories');

    Route::resource('cuadre-rutas', RouteCheckHistoryController::class)
        ->parameters(['cuadreRutas'=>'routeCheckHistory'])->only(['store','index'])
        ->names('routeCheckHistories');

    //Rutas cobrador

    Route::get('mis-rutas',[RouteController::class,'routeCobrador'])
        ->name('routes.routeCobrador');

    Route::get('mis-rutas/{route}/verificar',[RouteController::class,'consultPendingLoansInRoute'])
        ->name('routes.consultPendingLoansInRoute');

    Route::get('mis-rutas/{route}/cobrando',[RouteController::class,'collect'])
        ->name('routes.collect');

    Route::get('mis-rutas/{loand}/pagos',[PaymentController::class,'paymentOfLoandPending'])
        ->name('payments.paymentOfLoandPending');

    Route::post('mis-rutas/{route}/cobrando/{loand}/visitar',[RouteController::class,'checkRouteLoand'])
        ->name('payments.checkRouteLoand');

    //Reportes

    Route::get('reportes',[ReportController::class,'menu'])
        ->name('reports.menu');

    Route::get('reportes/prestamos-activos',[ReportController::class,'activeLoans'])
        ->name('reports.activeLoans');

    Route::get('reportes/prestamos-activos-list',[ReportController::class,'activeLoansList'])
        ->name('reports.activeLoans.list');

    Route::get('reportes/prestamos-aprobados',[ReportController::class,'approvedLoans'])
        ->name('reports.approvedLoans');

    Route::get('reportes/prestamos-aprobados-list',[ReportController::class,'approvedLoansList'])
        ->name('reports.approvedLoans.list');

    Route::get('reportes/cuotas-atrasadas',[ReportController::class,'arrearsFees'])
        ->name('reports.arrearsFees');

    Route::get('reportes/cuotas-atrasadas-list',[ReportController::class,'arrearsFeesList'])
        ->name('reports.arrearsFees.list');

    Route::get('reportes/prestamos-pagados',[ReportController::class,'paidLoans'])
        ->name('reports.paidLoans');

    Route::get('reportes/prestamos-pagados-list',[ReportController::class,'paidLoansList'])
        ->name('reports.paidLoans.list');

    Route::get('reportes/prestamos-por-tipo',[ReportController::class,'typeLoans'])
        ->name('reports.typeLoans');

    Route::get('reportes/prestamos-por-tipo-list',[ReportController::class,'typeLoansList'])
        ->name('reports.typeLoans.list');


});


