@extends('errors::illustrated-layout')

@section('title', __('Forbidden'))
@section('message', 'Acceso no autorizado')
@section('code', '403')
@section('image')
    <img src="{{asset('images/403.png')}}"  alt="403 imagen"  class="img-403">
@endsection
