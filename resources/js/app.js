require('./bootstrap');

import Vue from 'vue'
import { createInertiaApp } from '@inertiajs/inertia-vue'
import vuetify from './Plugins/vuetify'
import AppLayout from './Shared/Layouts/AppLayout'
import DefaultLayout from './Shared/Layouts/DefaultLayout'
import VueMeta from 'vue-meta'
import { InertiaProgress } from '@inertiajs/progress'
import Croppa from 'vue-croppa'
import 'boxicons'

Vue.use(VueMeta)
Vue.use(Croppa)
Vue.mixin({methods:{ route }});
InertiaProgress.init({
    color: '#26c6da',
})
createInertiaApp({
    resolve: name => require(`./Pages/${name}`),
    setup({ el, App, props }) {
        new Vue({
            vuetify,
           /* render: h => h(App, props),*/
            render: h => h(App, {
                props: {
                    initialPage: JSON.parse(el.dataset.page),
                    resolveComponent: name => import(`./Pages/${name}`)
                        .then(({ default: page }) => {
                            if (page.layout === undefined && name.startsWith('Public/')) {
                                page.layout = DefaultLayout
                            }
                            if (page.layout === undefined && name.startsWith('Private/')) {
                                page.layout = AppLayout
                            }
                            return page
                        }),
                },
            }),
        }).$mount(el)
    },
})
