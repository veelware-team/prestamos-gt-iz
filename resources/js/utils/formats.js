export function formatCurrencyGT(value) {

    let formato = new Intl.NumberFormat("es-GT", {
        style: "currency",
        currency: "GTQ",
        currencyDisplay: "narrowSymbol",
    }).format(value);

    return formato;
}
