export const loandTypes = [
    "DIARIO",
    "SEMANAL",
    "QUINCENAL",
    "MENSUAL",
];

export const loandTypesAll = [
    "TODOS",
    "DIARIO",
    "SEMANAL",
    "QUINCENAL",
    "MENSUAL",
];

export const loandStates = [
    "TODOS",
    "PENDIENTE",
    "APROBADO",
    "RECHAZADO",
    "ACTIVO",
    "PAGADO",
    "ANULADO",
    "REFINANCIADO",
];
