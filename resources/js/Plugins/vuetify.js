import Vue from 'vue'
import Vuetify from 'vuetify'
/*import 'vuetify/dist/vuetify.min.css'*/
import colors from 'vuetify/lib/util/colors'
import '@mdi/font/css/materialdesignicons.min.css'
Vue.use(Vuetify)
const opts = {
    theme: {
        icons: {
            iconfont: 'mdi', // default - only for display purposes
        },
        themes: {
            light: {
                primary:  colors.cyan.lighten1,
                secondary: '#162034',
                accent: '#82B1FF',
                /*error: colors.red.lighten1,
                info: '#2196F3',
                success: colors.green.lighten1,*/
                warning: '#FFC107',
                support: colors.grey.darken3,
            },
        },
    },
}

export default new Vuetify(opts)
